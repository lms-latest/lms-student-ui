// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


export const environment = {
  production: false,
  apiBaseUrl: 'https://localhost:44315',
  // apiBaseUrl: 'http://lms.imentus.com/webapi',
  courseImageUrl: 'assets/images/courses/',
  categoryImageUrl: 'assets/images/category/',
  videoUrl: 'assets/Uploads/TopicAttachment/Video/',
  pdfUrl: 'assets/Uploads/TopicAttachment/PDF/',
  audioUrl: 'assets/Uploads/TopicAttachment/Audio/',
  zipUrl: 'assets/Uploads/TopicAttachment/Zip/',
  topicIntervalTimer: 5000,
  srcUrlProductThumbnail: 'assets/Uploads/ProductThumbnail/',
  srcUrlCourseThumbnail: 'assets/Uploads/CourseThumbnail/',
  srcUrlProductCategory: 'assets/Uploads/Category/ImagesSmall/',
  featureProductUrl: 'assets/Uploads/FeatureImage/',
  userRole: 'user-role',
  // tslint:disable-next-line:max-line-length
  cryptoPublicKey: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCyKy5OVc/1PtMp04oOHbzVK8wcnss/XpxQVEe8yVntD1HvQx/AdK+zDcaAtg4vXUt9W3UaTJbPlcWg5nNz+Vi9+/8Mr+98IStqKWpQHM+xtwcbuS9VCyOD0wgZbMWAT9MIOUZEMBaFiyCsCjFjxjED5yylfgJozUPZMfvyQ8TcWQIDAQAB'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
