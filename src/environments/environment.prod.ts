export const environment = {
  production: true,
  apiBaseUrl: 'http://lms.imentus.com/webapi',
  courseImageUrl: 'assets/images/courses/',
  categoryImageUrl: 'assets/images/category/',
  videoUrl: 'assets/video/courses-liabrery/',
  pdfUrl: 'assets/pdf/',
  mp3Url: 'assets/mp3/',
  topicIntervalTimer: 5000,
  srcUrlProductThumbnail: 'assets/Uploads/ProductThumbnail/',
  srcUrlProductCategory: 'assets/Uploads/Category/ImagesSmall/',
  featureProductUrl: 'assets/Uploads/FeatureImage/',
  userRole: 'user-role',
  // tslint:disable-next-line:max-line-length
  cryptoPublicKey: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCyKy5OVc/1PtMp04oOHbzVK8wcnss/XpxQVEe8yVntD1HvQx/AdK+zDcaAtg4vXUt9W3UaTJbPlcWg5nNz+Vi9+/8Mr+98IStqKWpQHM+xtwcbuS9VCyOD0wgZbMWAT9MIOUZEMBaFiyCsCjFjxjED5yylfgJozUPZMfvyQ8TcWQIDAQAB'
};

