import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../../shared/services/orders.service';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { Order } from '../../shared/interface/orders';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {
  orderID: number;
  OrderBox: any;
  productThumbnailUrl = environment.srcUrlProductThumbnail;
  featureProductUrl = environment.featureProductUrl;
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private ordersService: OrdersService) {
    this.activatedRoute.params.subscribe(params => {
      this.orderID = params.id;
      console.log('this.orderID', this.orderID);
    });
  }

  ngOnInit() {
    this.getOrderDetails();
  }

  getOrderDetails() {
    console.log('getOrderDetails: ');
    this.ordersService.getOrderDetails(this.orderID ).subscribe((data: Order) => {
      console.log('Orders: ', data);
      this.OrderBox = data;
    });
  }

  viewProductDetails(url, id) {
    const productUrl = `${url}/${id}`;
    console.log('productUrl: ', productUrl);
    this.router.navigateByUrl(productUrl).then(e => {
      if (e) {
      } else {
      }
    });
  }
}
