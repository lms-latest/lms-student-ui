import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderDetailsRoutingModule } from './order-details-routing.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    OrderDetailsRoutingModule
  ]
})
export class OrderDetailsModule { }


