import { Component, OnInit, AfterViewInit, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { CourseService } from '../../shared/services/course.service';
import { Topic } from '../../shared/interface/topic';
import { TreeTopic } from '../../shared/interface/treetopic';
import { environment } from '../../../environments/environment';
import { TreeNode } from '../../shared/interface/treeNode';
import { TopicPacket } from '../../shared/interface/topicPacket';
import { interval, Subscription } from 'rxjs';
import { Course } from '../../shared/interface/course';

@Component({
  selector: 'app-course-details',
  templateUrl: './course-details.component.html',
  styleUrls: ['./course-details.component.css']
})
export class CourseDetailsComponent implements OnInit, OnDestroy, AfterViewInit {
  topicResults: Topic;
  courseID: number;
  nestedTopics: TreeTopic[];
  nestedTreeNode: TreeNode[];
  selectedNodes: TreeNode;
  videoUrl = environment.videoUrl;
  pdfUrl = environment.pdfUrl;
  mp3Url = environment.audioUrl;
  zipUrl = environment.zipUrl;
  topicIntervalTimer = environment.topicIntervalTimer;
  IsPreview = false;
  videoPreview = false;
  audioPreview = false;
  quizPreview = false;
  pdfPreview = false;
  selectedTopic: any;
  cols: any[];
  topicPacket: TopicPacket;
  mySubscription: Subscription;
  intervalId: number;
  timer = null;
  currentTopicID: number;
  quizPreviewPacket: any;
  CourseInfo: Course;
  srcUrlCourseThumbnail = environment.srcUrlCourseThumbnail;

  @ViewChild('topicVideo', { static: false }) topicVideo: ElementRef;
  QuizpreviewID: number;

  constructor(private activatedRoute: ActivatedRoute, private courseService: CourseService, private elRef: ElementRef) {
    this.activatedRoute.params.subscribe(params => {
      console.log(' params   params :  ', params);
      this.courseID = params.id;
    });
    this.topicResults = {
      ID: 0, Active: false, CreatedBy: 0, CreatedDate: new Date(), ModifiedBy: 0, CourseID: 0,
      ModifiedDate: new Date(), ParentTopicID: 0, TopicAttachment: '', TopicDescription: '', TopicPath: '', TopicTitle: '',
      NodeRefID: '', ParentTopicRefID: '', QuizID: 0, TopicFileName: '', TopicType: ''
    };
    this.nestedTopics = [{ ID: 0, label: '', data: '', type: '', IconName: '', children: this.nestedTopics }];
    this.nestedTreeNode = [{
      data: this.topicResults, children: [],
      leaf: false, expanded: false
    }];
    this.selectedTopic = { topicTitle: '', topicTypeID: '', topicUrl: '', topicDescription: '' };
    this.topicPacket = { ID: 0, CourseID: 0, TopicID: 0, TopicDuration: 0, TopicStatus: false };
    this.quizPreviewPacket = { QuizID: 0, CurrentTopicID: 0, };
  }

  ngOnInit() {
    // this.topicUrl = 'assets/images/courses/c plus.jpg';
    // this.getCourseDetails(this.courseID);
    this.getCourseTree(this.courseID);
    this.getCourseInfo(this.courseID);
    this.cols = [
      { field: 'TopicTitle', header: 'Name', width: '70%' },
      { field: 'TopicType', header: 'Type', width: '15%' },
      { field: '', header: 'Progress', width: '15%' }
    ];
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    this.stopTopicTimer();
  }

  startTopicTimer() {
    this.stopTopicTimer();
    this.timer = setInterval(this.saveTopicProgress, this.topicIntervalTimer);
  }

  stopTopicTimer() {
    if (this.timer) {
      clearInterval(this.timer);
    }
  }

  saveTopicProgress() {
    const videoInfo = this.elRef.nativeElement.querySelector('video');
    this.topicPacket.TopicDuration = videoInfo.currentTime.toFixed();
    this.topicPacket.TopicID = this.currentTopicID;
    this.topicPacket.CourseID = this.courseID;
    if (videoInfo.currentTime === videoInfo.duration) {
      this.topicPacket.TopicStatus = true;
    }
    this.courseService.saveTopicProgress(this.topicPacket).then((result: any) => {

    });
  }

  getCourseTree(courseID: number) {
    this.courseService.getCourseTree(courseID).subscribe((data: TreeNode[]) => {
      this.nestedTreeNode = data;
      // console.log('nestedTreeNode: ', this.nestedTreeNode);
    });
  }

  getCourseInfo(courseID: number) {
    this.courseService.getCourseInfo(courseID).subscribe((data: Course) => {
      this.CourseInfo = data;
      // console.log('getCourseInfo: ', this.CourseInfo);
    });
  }
  getCourseDetails(courseID: number) {
    this.courseService.getCourseDetails(courseID).subscribe((data: TreeTopic[]) => {
      this.nestedTopics = data;
    });
  }

  getFileExtension(filename: string) {
    // tslint:disable-next-line:no-bitwise
    return filename.slice((filename.lastIndexOf('.') - 1 >>> 0) + 2);
  }

  transformUrl(value: string, type: string) {
    switch (type) {
      case 'Audio': return this.mp3Url + value;
      case 'Video': return this.videoUrl + value;
      case 'PDF': return this.pdfUrl + value;
      case 'ZIP': return this.zipUrl + value;
      case 'Quiz': return null;
      default: throw new Error(`Invalid safe type specified: ${type}`);
    }
  }

  nodeSelect(event) {
    console.log('nodeSelect event.node.data', event.node.data);
    this.IsPreview = true;
    this.currentTopicID = event.node.data.ID;
    const ext = this.getFileExtension(event.node.data.TopicFileName);
    this.selectedTopic.topicUrl = this.transformUrl(event.node.data.TopicFileName, event.node.data.TopicType);
    this.selectedTopic.topicTitle = event.node.data.TopicTitle;
    this.selectedTopic.topicType = event.node.data.TopicType;
    this.selectedTopic.topicDescription = event.node.data.TopicDescription;
    this.QuizpreviewID = event.node.data.QuizID;
    // this.startTopicTimer();
  }

  nodeUnselect(event) {
    console.log('nodeUnSelect event', event.node);
  }

  stripHtml(html) {
    const tmp = document.createElement('DIV');
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || '';
  }

}
