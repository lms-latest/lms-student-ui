import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CourseDetailsRoutingModule } from './course-details-routing.module';
import { TreeModule } from 'primeng/tree';
// import { SafePipe } from '../../shared/pipes/safe.pipe';



@NgModule({
  imports: [
    TreeModule,
    CommonModule,
    CourseDetailsRoutingModule],
    // exports: [SafePipe]
})

export class CourseDetailsModule { }
