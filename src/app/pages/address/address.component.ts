import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { Address, AddressType, AddressBasket, State, Country } from '../../shared/interface/address';
import { AddressService } from '../../shared/services/address.service';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit {

  address: Address;
  addressEmpty: Address;
  addressBasket: AddressBasket;
  addressList: Address[];
  addressType = [];
  state: State[];
  country: Country[];
  countryItems = [];
  stateItems = [];
  isShowAddressForm: boolean;
  cartID: string;
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private addressService: AddressService) {
    this.address = {
      ID: 0, FullName: '', MobileNumber: '', AlternatPhoneNumber: '', Pincode: '', HouseBuildingCompanyApartment: '',
      AreaColonyStreetSectorVillage: '', Landmark: '', TownCityDistrict: '', State: '', Country: '', StateID: 0, CountryID: 0,
      IsDefaultAddress: false, AddressTypeID: 0
    };

    this.addressEmpty = {
      ID: 0, FullName: '', MobileNumber: '', AlternatPhoneNumber: '', Pincode: '', HouseBuildingCompanyApartment: '',
      AreaColonyStreetSectorVillage: '', Landmark: '', TownCityDistrict: '', State: '', Country: '', StateID: 0, CountryID: 0,
      IsDefaultAddress: false, AddressTypeID: 0
    };
    this.addressBasket = { ListAddress: [this.address], addressType: this.addressType };
    // this.addressType = [
    //   { name: 'Billing Address', id: 1 },
    //   { name: 'Delivery Address', id: 2 }
    // ];
    this.activatedRoute.params.subscribe(params => {
      this.cartID = params.id;
      console.log('cartID', this.cartID);
    });
  }

  ngOnInit() {
    this.getAddress();
    this.getCountry();
    this.getAddressType();
  }

  getAddress() {
    this.addressService.getAddress().subscribe((data: AddressBasket) => {
      console.log('data getAddress: ', data);
      this.addressList = data.ListAddress;
    });
  }

  getCountry() {
    this.addressService.getCountry().subscribe((data: any) => {
      this.countryItems = [];
      data.forEach(item => {
        this.countryItems.push({
          label: item.Name,
          value: item.ID,
        });
      });
    });
  }

  getAddressType() {
    this.addressService.getAddressType().subscribe((data: any) => {
      this.addressType = [];
      data.forEach(item => {
        this.addressType.push({
          label: item.Name,
          value: item.ID,
        });
      });
    });
  }

  StateByCountryID() {
    console.log('this.address.CountryID', this.address.CountryID);
    this.addressService.getStateByCountryID(this.address.CountryID).subscribe((data: any) => {
      this.stateItems = [];
      data.forEach(item => {
        this.stateItems.push({
          label: item.Name,
          value: item.ID,
        });
      });
    });
  }


  SaveAddress() {
    console.log('this.address', this.address);
    this.addressService.SaveAddress(this.address).then((result: any) => {
      this.isShowAddressForm = false;
      this.getAddress();
    });
  }

  Cancel() {
    this.address = this.addressEmpty;
  }

  DeliveryAddress(url, id) {
    const checkoutUrl = `${url}/${id}`;
    this.router.navigateByUrl(checkoutUrl).then(e => {
    });
  }

  EditAddress(address) {
    this.address = address;
    this.StateByCountryID();
    this.isShowAddressForm = true;
  }

  DeleteAddress(addressID) {
    this.addressService.DeleteAddress(addressID).subscribe((data: boolean) => {
      this.getAddress();
    });
  }

  ShowAddressForm() {
    this.isShowAddressForm = true;
  }

  // GetCountry() {
  //   this.orderService.GetCountry().subscribe(data => {
  //     this.countryItems = [];
  //     data.forEach(user => {
  //       const countryItem = {
  //         label: user.Name,
  //         value: user.ID
  //       };
  //       this.countryItems.push(countryItem);
  //     });
  //   }, err => {
  //     console.log(err);
  //   });
  // }

}
