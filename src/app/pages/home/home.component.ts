import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from '../../shared/services/product.service';
import { CartService } from '../../shared/services/cart.service';
import { Product, ProductCategory, ProductFeatureValue, ProductTypeFeature } from '../../shared/interface/product';
import { Cart, CartDetails } from '../../shared/interface/cart';
import { Category } from '../../shared/interface/category';
import { environment } from '../../../environments/environment';
import { isGeneratedFile } from '@angular/compiler/src/aot/util';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  productDetail: Product;
  cart: Cart;
  cartDetails: CartDetails;
  allProduct: Product[];
  selectedProduct: Product;
  selectedProductFeature: ProductTypeFeature[];
  ListProductFeatureValue: ProductFeatureValue;
  productCategory: ProductCategory;
  allCategory: Category[];
  recommendedCategory: Category[];
  selectedCategory: Category;
  rootCategory: any[];
  subCategory: Category[];
  showSubCategory = false;
  productThumbnailUrl = environment.srcUrlProductThumbnail;
  featureProductUrl = environment.featureProductUrl;
  srcUrlProductCategory = environment.srcUrlProductCategory;
  productDetailsUrl: string;
  display: boolean;
  isLoggedin: string;
  constructor(
    private router: Router, private activatedRoute: ActivatedRoute,
    private productService: ProductService, private cartService: CartService) {
    this.activatedRoute.params.subscribe(params => {
      // this.callEvent = params.key;
    });
    this.selectedProduct = {
      ID: 0, ProductName: '', ProductDescription: '', SalePrice: '', Price: '', ProductImage: '', ProductTypeID: 0, CourseID: 0,
      ProgramID: 0, Active: false, ListProductTypeFeature: this.selectedProductFeature, ModifiedDate: new Date(),
      ProductCategories: [{ ID: 0, ProductID: 0, CategoryID: 0 }]
    };
    this.allCategory = [{
      ID: 0, Name: '', CategoryName: '', CategoryCode: '', IconName: '', ImageSmall: '', ImageMedium: '', ImageLarge: '',
      ParentCategoryID: 0, ParentName: ''
    }];
    this.selectedCategory = {
      ID: 0, Name: '', CategoryName: '', CategoryCode: '', IconName: '', ImageSmall: '', ImageMedium: '', ImageLarge: '',
      ParentCategoryID: 0, ParentName: ''
    };
    this.selectedProductFeature = [{
      ID: 0, DisplayName: '', FeatureImage: '', FeatureTypeID: 0, FeatureTypeName: '', IsDisplayToEndUser: false, Name: '',
      ProductTypeID: 0, ProductTypeFeatureID: 0, ListProductFeatureValue: [{ ID: 0, OptionName: '', Value: '', OptionID: 0 }]
    }];
    this.ListProductFeatureValue = { ID: 0, OptionName: '', Value: '', OptionID: 0 };
    this.cart = {
      ID: 0, UserID: 0, GuidID: '', Active: false, Subtotal: 0, Total: 0, TotalTax: 0,
      ListCartDetails: [{ ID: 0, CartID: 0, ProductID: 0, ProductName: '', ProductTypeID: 0,
       Price: '', SalePrice: '', Quantity: 0, productDetail: this.selectedProduct }]
    };

    this.cartDetails = { ID: 0, CartID: 0, ProductID: 0, ProductName: '', ProductTypeID: 0,
     Price: '', SalePrice: '', Quantity: 0, productDetail: this.selectedProduct };
  }

  ngOnInit() {
    this.getProduct();
    this.getCategoryList();
    this.getRecommendedCategory();
  }

  loginEvent($event) {
    this.display = $event;
    this.isLoggedin = 'true';
  }
  getProduct() {
    const productID = 0;
    this.productService.getProduct(productID).subscribe((data: Product[]) => {
      this.allProduct = data;
    });
  }

  getCategoryList() {
    this.productService.getCategoryList().subscribe((data: Category[]) => {
      this.allCategory = data;
      console.log('allCategory: ', this.allCategory);
      this.rootCategory = [];
      data.forEach((obj, index) => {
        if (obj.ParentCategoryID === null || obj.ParentCategoryID === 0) {
          console.log('rootCategory: ', obj);
          this.rootCategory.push(obj);
        }
      });
    });
  }

  getRecommendedCategory() {
    this.productService.getRecommendedCategory().subscribe((data: Category[]) => {
      this.recommendedCategory = data;
    });
  }

  viewProduct(url, id) {
    this.productDetailsUrl = `${url}/${id}`;
    this.router.navigateByUrl(this.productDetailsUrl).then(e => {
      if (e) {
        // this.saveVisitHistory(id);
      } else {
      }
    });
  }
  overlayEventSubcategory($event: any, category: Category) {
    this.selectedCategory = category;
    this.getChildCategory(category);
    if (this.subCategory.length > 0) {
      this.showSubCategory = true;
    } else {
      this.showSubCategory = false;
    }
  }

  getChildCategory(category) {
    // console.log('category: ', category);
    this.subCategory = [];
    this.allCategory.forEach((obj, index) => {
      if (category.ID === obj.ParentCategoryID) {
        this.subCategory.push(obj);
      }
    });
  }
  subCategoryScreenHide() {
    this.showSubCategory = false;
  }

  // overlayEventAllProduct(opAllProduct: any, $event: any, productFeatureValue: ProductFeatureValue[]) {
  //   console.log('productFeatureValue', productFeatureValue);
  //   this.selectedProductFeature = productFeatureValue;
  //   opAllProduct.show($event);
  // }
  // tslint:disable-next-line:max-line-length
  overlayEventAllProduct(opAllProduct: any, $event: any, product: Product) {
    // console.log('productFeatureValue', product);
    this.selectedProductFeature = product.ListProductTypeFeature;
    this.selectedProduct = product;
    opAllProduct.toggle($event);
  }

  addToCart(selectedProduct) {
    this.cart.ListCartDetails = [];
    this.cartDetails = { ID: 0, CartID: 0, ProductID: 0, ProductName: '', ProductTypeID: 0,
     Price: '', SalePrice: '', Quantity: 0, productDetail: this.selectedProduct };
    this.cartDetails.ProductID = selectedProduct.ID;
    this.cartDetails.ProductName = selectedProduct.ProductName;
    this.cartDetails.ProductTypeID = selectedProduct.ProductTypeID;
    this.cartDetails.Price = selectedProduct.Price;
    this.cartDetails.SalePrice = selectedProduct.SalePrice;
    this.cartDetails.Quantity = 1;
    this.cart.ListCartDetails.push(this.cartDetails);
    console.log('cart: ', this.cart);

    this.cartService.addToCart(this.cart).then((result: any) => {
      const cartCount = 1 + Number(this.cartService.Count);
      this.cartService.Count.next(cartCount.toString());
    });
  }


}
