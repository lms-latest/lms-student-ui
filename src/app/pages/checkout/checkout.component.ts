import { Component, OnInit } from '@angular/core';
import { IPayPalConfig, ICreateOrderRequest } from 'ngx-paypal';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { AddressService } from '../../shared/services/address.service';
import { CartService } from '../../shared/services/cart.service';
import { PaymentService } from '../../shared/services/payment.service';
import { Cart, CartDetails, PaymentMethod } from '../../shared/interface/cart';
import { Product } from '../../shared/interface/product';
import { PaymentCluster } from '../../shared/interface/PaymentCluster';
import { Address, AddressType, AddressBasket } from '../../shared/interface/address';
import * as uuid from 'uuid';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})

export class CheckoutComponent implements OnInit {
  public payPalConfig?: IPayPalConfig;
  paymentCluster: PaymentCluster;
  address: Address;
  cart: Cart;
  cartCount: number;
  totalAmount: number;
  addressID: number;
  ListProduct: Product;
  paymentSuccessUrl: string;
  paymentFailedUrl: string;
  transactionID: string;
  url: string;
  cartID: number;
  types: PaymentMethod[];
  selectedType: string;
  constructor(
    private router: Router, private activatedRoute: ActivatedRoute, private cartService: CartService,
    private addressService: AddressService, private paymentService: PaymentService) {
    // this.types = [
    //     {label: 'Apartment', value: 'Apartment'},
    //     {label: 'House', value: 'House'},
    //     {label: 'Studio', value: 'Studio'}
    // ];
    this.activatedRoute.params.subscribe(params => {
      console.log(' params   params :  ', params);
      this.addressID = params.id;
    });
    this.cart = {
      ID: 0, UserID: 0, GuidID: '', Active: false, Subtotal: 0, Total: 0, TotalTax: 0,
      ListCartDetails: [{
        ID: 0, CartID: 0, ProductID: 0, ProductName: '', ProductTypeID: 0,
        Price: '', SalePrice: '', Quantity: 0, productDetail: this.ListProduct
      }]
    };
    this.address = {
      ID: 0, FullName: '', MobileNumber: '', AlternatPhoneNumber: '', Pincode: '', HouseBuildingCompanyApartment: '',
      AreaColonyStreetSectorVillage: '', Landmark: '', TownCityDistrict: '', State: '', Country: '', StateID: 0, CountryID: 0,
      IsDefaultAddress: false, AddressTypeID: 0
    };
    this.paymentCluster = {
      ID: 0, UserID: 0, CardHolderName: '', CardNumber: '', CVV: '', ExpireMonth: '', ExpireYear: ''
      , TransactionAmount: '', TransactionID: '', CardType: 'VISA', TotalAmount: '', Currency: 'USD',
      PaymentMethod: 'paypal', AddressID: this.addressID, Cart: this.cart, BillingAddress: this.address
    };
    this.paymentSuccessUrl = '/paymentsuccess';
    this.paymentFailedUrl = '/paymentfail';
    this.transactionID = uuid.v4();
  }

  ngOnInit() {
    this.getCartDetails();
    this.getSelectedAddress();
    this.getPaymentMethod();
  }

  getCartDetails() {
    this.cartService.getCartDetails().subscribe((data: Cart) => {
      this.cart = data;
      this.paymentCluster.Cart = data;
      this.cartCount = data.ListCartDetails.length;
    });
  }

  getSelectedAddress() {
    this.addressService.getSelectedAddress(this.addressID).subscribe((data: Address) => {
      console.log('getSelectedAddress: ', data);
      this.paymentCluster.BillingAddress = data;
    });
  }

  getPaymentMethod() {
    this.cartService.getPaymentMethod().subscribe((data: any) => {
      console.log('getPaymentMethod: ', data);
      this.types = [];
      data.forEach(item => {
        this.types.push({
          label: item.Name,
          value: item.ID,
        });
      });
    });
  }

  checkoutPayment() {
    // console.log('paymentCluster', this.paymentCluster);
    this.paymentService.CheckoutPayment(this.paymentCluster).then((result: any) => {
      this.cartID = this.cart.ID;
      localStorage.removeItem('guidID');
      if (result === true) {
        this.url = '/paymentsuccess';
        const checkoutUrl = `${this.url}/${this.addressID}/${this.cartID}`;
        this.router.navigateByUrl(checkoutUrl).then(e => {
        });
        // this.router.navigate(['/paymentsuccess']);
      } else {
        this.url = '/paymentfail';
        const checkoutUrl = `${this.url}/${this.addressID}/${this.cartID}`;
        this.router.navigateByUrl(checkoutUrl).then(e => {
        });
      }
    });
  }
}

