import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {
  @Output() logoutEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
  display: boolean;
  constructor(private router: Router) { }

  ngOnInit() {
    // localStorage.removeItem('authToken');
    // localStorage.removeItem('userObject');
    // localStorage.removeItem('currentUser');
    // localStorage.removeItem('isLoggedin');
    // this.display = false;
    // this.logoutEvent.emit(this.display);
    // this.router.navigate(['/home']);
    // console.log('LogoutComponent');
    // console.log('logoutEvent');
  }

  // logout() {
  //   localStorage.removeItem('authToken');
  //   localStorage.removeItem('userObject');
  //   localStorage.removeItem('currentUser');
  //   localStorage.removeItem('isLoggedin');
  //   this.display = false;
  //   this.logoutEvent.emit(this.display);
  //   this.router.navigate(['/home']);
  // }

}
