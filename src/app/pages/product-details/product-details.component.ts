import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from '../../shared/services/product.service';
import { CartService } from '../../shared/services/cart.service';
import { Product, ProductCategory, ProductFeatureValue, ProductTypeFeature } from '../../shared/interface/product';
import { Cart, CartDetails } from '../../shared/interface/cart';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
  productDetail: Product;
  cart: Cart;
  cartDetails: CartDetails;
  productFeature: ProductTypeFeature[];
  productID: number;
  productThumbnailUrl = environment.srcUrlProductThumbnail;
  featureProductUrl = environment.featureProductUrl;
  viewCartUrl: string;
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private productService: ProductService,
              private cartService: CartService) {
    this.activatedRoute.params.subscribe(params => {
      this.productID = params.id;
    });
    this.productDetail = {
      ID: 0, ProductName: '', ProductDescription: '', SalePrice: '', Price: '', ProductImage: '', ProductTypeID: 0, CourseID: 0,
      ProgramID: 0, Active: false, ListProductTypeFeature: this.productFeature, ModifiedDate: new Date(),
      ProductCategories: [{ ID: 0, ProductID: 0, CategoryID: 0 }]
    };

    this.productFeature = [{
      ID: 0, DisplayName: '', FeatureImage: '', FeatureTypeID: 0, FeatureTypeName: '', IsDisplayToEndUser: false, Name: '',
      ProductTypeID: 0, ProductTypeFeatureID: 0, ListProductFeatureValue: [{ ID: 0, OptionName: '', Value: '', OptionID: 0 }]
    }];
    this.cart = {
      ID: 0, UserID: 0, GuidID: '', Active: false, Subtotal: 0, Total: 0, TotalTax: 0,
      ListCartDetails: [{ ID: 0, CartID: 0, ProductID: 0, ProductName: '', ProductTypeID: 0,
      Price: '', SalePrice: '', Quantity: 0, productDetail: this.productDetail}]
    };
  }

  ngOnInit() {
    this.getProductDetails();
  }


  getProductDetails() {
    const productID = this.productID;
    console.log('productID :  ', productID);
    this.productService.getProduct(productID).subscribe((data: Product) => {
      this.productDetail = data[0];
      console.log('this.productDetail :  ', this.productDetail);
    });
  }

  viewCartProduct(url, id) {
    // this.addToCart();
    this.viewCartUrl = `${url}/${id}`;
    console.log('viewCartUrl: ', this.viewCartUrl);
    // this.router.navigateByUrl(this.viewCartUrl).then(e => {
    //   if (e) {
    //     // this.saveVisitHistory(id);
    //   } else {
    //   }
    // });
  }

  addToCart(productDetail) {
    this.cart.ListCartDetails = [];
    this.cartDetails = { ID: 0, CartID: 0, ProductID: 0, ProductName: '', ProductTypeID: 0,
     Price: '', SalePrice: '', Quantity: 0, productDetail: this.productDetail };
    this.cartDetails.ProductID = productDetail.ID;
    this.cartDetails.ProductName = productDetail.ProductName;
    this.cartDetails.ProductTypeID = productDetail.ProductTypeID;
    this.cartDetails.Price = productDetail.Price;
    this.cartDetails.SalePrice = productDetail.SalePrice;
    this.cartDetails.Quantity = 1;
    this.cart.ListCartDetails.push(this.cartDetails);
    console.log('cart: ', this.cart);
    this.cartService.addToCart(this.cart).then((result: any) => {
      const cartCount = 1 + Number(this.cartService.Count);
      this.cartService.Count.next(cartCount.toString());
    });
  }

}
