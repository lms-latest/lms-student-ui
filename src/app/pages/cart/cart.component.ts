import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CartService } from '../../shared/services/cart.service';
import { Cart, CartDetails } from '../../shared/interface/cart';

import { Product, ProductCategory, ProductFeatureValue, ProductTypeFeature } from '../../shared/interface/product';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  cart: Cart;
  cartCount: number;
  ListProduct: Product;
  productFeature: ProductTypeFeature[];
  callEvent: any;
  constructor(
    private router: Router, private activatedRoute: ActivatedRoute,
    private cartService: CartService) {
    this.activatedRoute.params.subscribe(params => {
      console.log('cart activatedRoute params', params);
      if (params.key === 'callMethod') {
        this.getCartDetails();
      }
    });
    this.ListProduct = {
      ID: 0, ProductName: '', ProductDescription: '', SalePrice: '', Price: '', ProductImage: '', ProductTypeID: 0, CourseID: 0,
      ProgramID: 0, Active: false, ListProductTypeFeature: this.productFeature, ModifiedDate: new Date(),
      ProductCategories: [{ ID: 0, ProductID: 0, CategoryID: 0 }]
    };
    this.productFeature = [{
      ID: 0, DisplayName: '', FeatureImage: '', FeatureTypeID: 0, FeatureTypeName: '', IsDisplayToEndUser: false, Name: '',
      ProductTypeID: 0, ProductTypeFeatureID: 0, ListProductFeatureValue: [{ ID: 0, OptionName: '', Value: '', OptionID: 0 }]
    }];
    this.cart = {
      ID: 0, UserID: 0, GuidID: '', Active: false, Subtotal: 0, Total: 0, TotalTax: 0,
      ListCartDetails: [{
        ID: 0, CartID: 0, ProductID: 0, ProductName: '', ProductTypeID: 0,
        Price: '', SalePrice: '', Quantity: 0, productDetail: this.ListProduct
      }]
    };
  }

  ngOnInit() {
    this.cartService.Count.subscribe((count: string) => {
      this.getCartDetails();
    });
  }

  getCartDetails() {
    this.cartService.getCartDetails().subscribe((data: Cart) => {
      this.cart = data;
      this.cartCount = data.ListCartDetails.length;
      console.log('data :  ', data);
    });
  }

  resetCartCount() {
    this.cartCount = null;
  }
}
