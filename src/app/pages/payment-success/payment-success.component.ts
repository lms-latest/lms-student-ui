import { Component, OnInit } from '@angular/core';
import { ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AddressService } from '../../shared/services/address.service';
import { Address, AddressType, AddressBasket } from '../../shared/interface/address';
import { Cart, CartDetails } from '../../shared/interface/cart';
import { CartService } from '../../shared/services/cart.service';
import { Product } from '../../shared/interface/product';
import * as jsPDF from 'jspdf';

@Component({
  selector: 'app-payment-success',
  templateUrl: './payment-success.component.html',
  styleUrls: ['./payment-success.component.css']
})
export class PaymentSuccessComponent implements OnInit {

  addressID: number;
  cartID: number;
  address: Address;
  cart: Cart;
  ListProduct: Product;
  @ViewChild('invoicePOS') invoicePOS: ElementRef;
  constructor(
    private router: Router, private activatedRoute: ActivatedRoute,
    private addressService: AddressService, private cartService: CartService) {
    this.activatedRoute.params.subscribe(params => {
      console.log(' params   params :  ', params);
      this.addressID = params.aid;
      this.cartID = params.cid;
    });
    this.cart = {
      ID: 0, UserID: 0, GuidID: '', Active: false, Subtotal: 0, Total: 0, TotalTax: 0,
      ListCartDetails: [{
        ID: 0, CartID: 0, ProductID: 0, ProductName: '', ProductTypeID: 0,
        Price: '', SalePrice: '', Quantity: 0, productDetail: this.ListProduct
      }]
    };
    this.address = {
      ID: 0, FullName: '', MobileNumber: '', AlternatPhoneNumber: '', Pincode: '', HouseBuildingCompanyApartment: '',
      AreaColonyStreetSectorVillage: '', Landmark: '', TownCityDistrict: '', State: '', Country: '', StateID: 0, CountryID: 0,
      IsDefaultAddress: false, AddressTypeID: 0
    };
  }

  ngOnInit() {
    this.getSelectedAddress();
    this.getCartDetailsByID();
  }

  public openPDF(): void {
    const DATA = this.invoicePOS.nativeElement;
    const doc = new jsPDF('p', 'pt', 'a4');
    doc.fromHTML(DATA.innerHTML, 15, 15);
    doc.output('dataurlnewwindow');
  }

  public downloadPDF(): void {
    const DATA = this.invoicePOS.nativeElement;
    const doc = new jsPDF('p', 'pt', 'a4');

    const handleElement = {
       // tslint:disable-next-line:object-literal-shorthand
      // tslint:disable-next-line:only-arrow-functions
      '#editor'(element, renderer) {
        return true;
      }
    };
    doc.fromHTML(DATA.innerHTML, 15, 15, {
      width: 200,
      elementHandlers: handleElement
    });

    doc.save('angular-demo.pdf');
  }

  getSelectedAddress() {
    this.addressService.getSelectedAddress(this.addressID).subscribe((data: Address) => {
      console.log('getSelectedAddress: ', data);
      this.address = data;
    });
  }

  getCartDetailsByID() {
    this.cartService.getCartDetailsByID(this.cartID).subscribe((data: Cart) => {
      console.log('GetCartDetailsByID: ', data);
      this.cart = data;
    });
  }

}
