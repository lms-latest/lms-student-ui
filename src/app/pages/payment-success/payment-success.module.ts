import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentSuccessRoutingModule } from './payment-success-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PaymentSuccessRoutingModule
  ]
})
export class PaymentSuccessModule { }
