import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../../shared/services/orders.service';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { Order } from '../../shared/interface/orders';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  Orders: Order[];
  productThumbnailUrl = environment.srcUrlProductThumbnail;
  featureProductUrl = environment.featureProductUrl;
  constructor(private ordersService: OrdersService, private router: Router) {
    console.log('constructor getOrdersDetails: ');
  }

  ngOnInit() {
    this.getOrdersList();
  }

  getOrdersList() {
    console.log('getOrdersList: ');
    this.ordersService.getOrders().subscribe((data: Order[]) => {
      console.log('Orders: ', data);
      this.Orders = data;
    });
  }

  viewProductDetails(url, id) {
    const productUrl = `${url}/${id}`;
    console.log('productUrl: ', productUrl);
    this.router.navigateByUrl(productUrl).then(e => {
      if (e) {
      } else {
      }
    });
  }

  viewOrderDetails(url, id) {
    const orderDetailsUrl = `${url}/${id}`;
    console.log('orderDetailsUrl: ', orderDetailsUrl);
    this.router.navigateByUrl(orderDetailsUrl).then(e => {
      if (e) {
      } else {
      }
    });
  }


  overlayEventAddress(opAddress: any, $event: any) {
    opAddress.toggle($event);
  }

  overlayEventAddressHide(opAddress: any, $event: any) {
    opAddress.hide($event);
  }

}
