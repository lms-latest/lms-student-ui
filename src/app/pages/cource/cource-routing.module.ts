import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CourceComponent } from './cource.component';
import { Routes, RouterModule } from '@angular/router';



const routes: Routes = [
  {
    path: '',
    component: CourceComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CourceRoutingModule { }
