import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CourceRoutingModule } from './cource-routing.module';


@NgModule({
  imports: [
      CommonModule,
      CourceRoutingModule]
})
export class CourceModule { }
