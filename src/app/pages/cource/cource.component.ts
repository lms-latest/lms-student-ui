import { Component, OnInit } from '@angular/core';
import { Router  } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { CourseService } from '../../shared/services/course.service';
import {SelectItem} from 'primeng/api';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-cource',
  templateUrl: './cource.component.html',
  styleUrls: ['./cource.component.css']
})
export class CourceComponent implements OnInit {
  safeSrc: SafeResourceUrl;
  videoUrl = environment.videoUrl;
  topicObject = [];
  cars: SelectItem[];
  cities = [];

  constructor(
    private route: ActivatedRoute,
    private courseService: CourseService,
    // private router: Router,
    private sanitizer: DomSanitizer) {
    // this.route.paramMap.pipe(map(paramMap => paramMap.get('id')));
    this.route.params.subscribe( params => console.log(' params   params :  ', params) );
    this.safeSrc =  this.sanitizer.bypassSecurityTrustResourceUrl( this.videoUrl
      + 'Angular_8_(LOGIN PAGE).mp4' + '?autoplay=0' );
    // this.safeSrc =  this.videoUrl + 'Angular_8_(LOGIN PAGE).mp4';
    // console.log('safeSrc: ', this.safeSrc);
  }

  ngOnInit() {
    this.getTopicsDetails();
  }

  getTopicsDetails() {
    // this.courseService.getTopicsDetails(4).subscribe((data: any[]) => {
    //   this.topicObject = data;
    //   console.log('showAllCources: ', this.topicObject);
    // });
  }

}
