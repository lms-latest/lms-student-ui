import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowResultRoutingModule } from './show-result-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ShowResultRoutingModule
  ]
})
export class ShowResultModule { }
