import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowResultComponent } from './show-result.component';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    component: ShowResultComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShowResultRoutingModule { }

