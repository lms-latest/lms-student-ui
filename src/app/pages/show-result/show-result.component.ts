import { Component, OnInit, Input } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-show-result',
  templateUrl: './show-result.component.html',
  styleUrls: ['./show-result.component.css']
})
export class ShowResultComponent implements OnInit {

  @Input() CorrectAnswer: number;
  @Input() WrongAnswer: number;
  @Input() TotalQuestions: number;
  @Input() TotalMarks: number;
  @Input() isShowResult: boolean;
  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    console.log('CorrectAnswer', this.CorrectAnswer);
    console.log('WrongAnswer', this.WrongAnswer);
    console.log('TotalQuestions', this.TotalQuestions);
    console.log('TotalMarks', this.TotalMarks);
    console.log('isShowResult', this.isShowResult);
  }

}
