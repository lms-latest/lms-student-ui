import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchresultComponent } from './searchresult.component';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    component: SearchresultComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchresultRoutingModule { }

