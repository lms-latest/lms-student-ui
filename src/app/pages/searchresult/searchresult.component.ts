import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CourseService } from '../../shared/services/course.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { Subject, BehaviorSubject } from 'rxjs';
import { SearchService } from '../../shared/services/search.service';
import { PagedResult } from '../../shared/interface/pagedResult';
import { Course } from '../../shared/interface/course';
import { Topic } from '../../shared/interface/topic';
@Component({
  selector: 'app-searchresult',
  templateUrl: './searchresult.component.html',
  styleUrls: ['./searchresult.component.css']
})
export class SearchresultComponent implements OnInit {
  courseImageUrl = environment.courseImageUrl;
  searchKey: string;
  searchCourse: Course[];
  pagedResult: PagedResult<Course>;
  pageSize: number;
  topic: Topic;
  @Output() callParentFun: EventEmitter<any> = new EventEmitter<any>();

  constructor(private courseService: CourseService, private activatedRoute: ActivatedRoute, private searchService: SearchService) {
    this.activatedRoute.params.subscribe(params => {
      this.searchKey = params.key;
    });
    this.pageSize = 3;
    this.searchCourse = [];
    this.pagedResult = { Count: 0, Result: this.searchCourse };
  }

  ngOnInit() {
    this.searchService.searchTerm.subscribe((searchTerm: string) => {
      if (searchTerm != null) {
        this.searchKey = searchTerm;
        this.getAllSearchResult(searchTerm, 1, this.pageSize);
      }
    });
  }

  getAllSearchResult(searchKey: string, pageNumber: number, pageSize: number) {
    this.searchKey = searchKey;
    this.courseService.getAllSearchResult(searchKey, pageNumber, pageSize).subscribe((data: PagedResult<Course>) => {
      this.pagedResult.Count = data.Count;
      this.pagedResult.Result = data.Result;
      console.log('this.pagedResult.Result ', this.pagedResult.Result );
    });
  }

  loadData(event) {
    const pageNumber = (event.first / event.rows) + 1;
    this.getAllSearchResult(this.searchKey, pageNumber, this.pageSize);
  }

}
