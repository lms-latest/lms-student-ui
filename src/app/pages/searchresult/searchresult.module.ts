import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchresultRoutingModule } from './searchresult-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SearchresultRoutingModule
  ]
})
export class SearchresultModule { }

