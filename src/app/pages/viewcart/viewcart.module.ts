import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewcartRoutingModule } from './viewcart-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ViewcartRoutingModule
  ]
})
export class ViewcartModule { }
