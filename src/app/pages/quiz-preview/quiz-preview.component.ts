import { Component, OnInit, Input } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { MessageService, SelectItem } from 'primeng/api';
import { environment } from 'src/environments/environment';
import { QuizService } from 'src/app/shared/services/quiz.service';
import { Quiz, QuestionType, Answer, QuestionOption, Question } from '../../shared/interface/quiz';
import { ConfirmationDialog } from '../../shared/interface/confirmationDialog';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-quiz-preview',
  templateUrl: './quiz-preview.component.html',
  styleUrls: ['./quiz-preview.component.css']
})
export class QuizPreviewComponent implements OnInit {
  msgs: ConfirmationDialog[];
  public quiz: Quiz;
  showSubmitButton: boolean;
  startQuizButton: boolean;
  quizSubmission: boolean;
  isSubmit = false;
  quizStart = false;
  isShowResult = false;
  totalRecords: number;
  responseAnswer: any;
  checked = true;
  TotalMarks: number;
  @Input() QuizID: number;
  @Input() CourseID: number;
  @Input() TopicID: number;



  constructor(
    private route: ActivatedRoute, private router: Router,
    private quizService: QuizService, private confirmationService: ConfirmationService) {
    this.quiz = {
      ID: 0, IsMarkApplicable: false, PassingMarks: '',
      QuestionList: [{
        ID: 0, QuestionTitle: '', HelpText: '', Marks: '', IsRequired: false, QuestionTypeID: 0, QuizID: 0,
        QuestionOptions: [{ ID: 0, Name: '', IsCorrect: false, QuestionID: 0, QuestionRefID: '', Active: false, IsChecked: false }],
        RefID: '', Active: false, Answer: { AnswerSingle: 0, AnswerText: '', AnswerMultiple: [] }
      }], QuestionPerPage: 0, QuizTitle: '', QuizDuration: 0, ShowCorrectAnswer: false, ShowResultAfterSubmission: false,
      isQuizSubmission: false, QuizSubmissionDuration: 0, QuizStartOn: new Date(), QuizEndOn: new Date(), TotalMarks: 0, GetMarks: 0
    };
    this.msgs = [{ severity: '', summary: '', detail: '' }];
    this.startQuizButton = false;
  }


  ngOnInit() {
    console.log('this.QuizID', this.QuizID);
    console.log('this.CourseID', this.CourseID);
    // this.getQuizSubmission(this.QuizID, this.CourseID);
    console.log('ngOnInit  getQuizCollection ');
    this.getQuizCollection(this.QuizID, this.CourseID);
    this.showSubmitButton = false;
  }



  // startQuiz() {
  //   // this.startQuizButton = true;
  //   this.quizStart = true;
  // }

  handleEvent(event) {
    console.log('handleEvent', event);
    console.log('second', event.i.value / 1000);

    if (event.action === 'done') {
      this.isSubmit = true;

      this.quiz.QuizDuration = event.i.value / 1000;
      // this.submitAnswer();
      event.stop();
    } else {
      this.quiz.QuizSubmissionDuration = this.quiz.QuizDuration - event.i.value / 1000;
    }
  }

  confirmStart(cd) {
    this.confirmationService.confirm({
      message: 'Are you sure to start quiz?   You have ' + this.quiz.QuizDuration / 60 + ' minutes only!',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.startQuizButton = true;
        this.quizStart = true;
        this.quiz.QuizStartOn = new Date();
        cd.begin();
      },
      reject: () => {
      }
    });
  }

  confirmToSubmit(cd) {
    this.confirmationService.confirm({
      message: 'Are you sure to submit quiz...?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        // this.startQuizButton = true;
        this.handleEvent(cd);
        this.quiz.QuizEndOn = new Date();
        this.isSubmit = true;
        cd.stop();
        this.submitAnswer();
        this.msgs = [{ severity: 'info', summary: 'Confirmed', detail: 'You have accepted ' }];
      },
      reject: () => {
        this.msgs = [{ severity: 'info', summary: 'Rejected', detail: 'You have rejected ' }];
      }
    });
  }


  showResult() {
    this.isShowResult = true;
  }
  submitAnswer() {
    // console.log('Submit Answer', this.quiz);
    this.responseAnswer = { Quiz: this.quiz, CourseID: this.CourseID, TopicID: this.TopicID };
    this.quizService.saveQuizResponseAnswer(this.responseAnswer).then((result: any) => {
    });
  }

  getQuizSubmission(quizID, courseID) {
    if (quizID !== 0) {
      this.quizService.getQuizSubmission(quizID, courseID).subscribe((data: boolean) => {
        this.quizSubmission = data;
        // this.totalRecords = data.QuestionList.length;
        console.log('\n \n this.quiz ', this.quiz);
        this.getQuizCollection(this.QuizID, this.CourseID);
      });
    }
  }

  getQuizCollection(quizId, courseID) {
    console.log('call getQuizCollection ');
    if (quizId !== 0) {
      this.quizService.getQuizByID(quizId, courseID).subscribe((data: Quiz) => {
        this.quiz = data;
        console.log('getQuizCollection data', data);
        this.TotalMarks = this.quiz.TotalMarks;
        this.totalRecords = data.QuestionList.length;
        // console.log('\n \n this.quiz ', this.quiz);
      });
    }
  }

  // paginate(event) {
  //   console.log(event);
  //   console.log(event.first, 'Index of the first record');
  //   console.log(event.rows, 'Number of rows to display in new page');
  //   console.log(this.totalRecords, 'before');
  //   this.totalRecords = this.totalRecords - event.rows;
  //   console.log(this.totalRecords , 'after');
  //   if (this.totalRecords <= 2) {
  //     this.showSubmitButton = true;
  //   }

  // }
}
