import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuizPreviewComponent } from './quiz-preview.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: QuizPreviewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class QuizPreviewRoutingModule { }


