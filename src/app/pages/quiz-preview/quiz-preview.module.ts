import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuizPreviewRoutingModule } from './quiz-preview-routing.module';



@NgModule({
  imports: [
    CommonModule,
    QuizPreviewRoutingModule]
})
export class QuizPreviewModule { }

