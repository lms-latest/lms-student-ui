import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartDetailsRoutingModule } from './cart-details-routing.module';



@NgModule({
  imports: [
      CommonModule,
      CartDetailsRoutingModule]
})
export class CartDetailsModule { }




