import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CartService } from '../../shared/services/cart.service';
import { Cart, CartDetails } from '../../shared/interface/cart';
import { Product, ProductCategory, ProductFeatureValue, ProductTypeFeature } from '../../shared/interface/product';
import { environment } from '../../../environments/environment';
import { DialogModule } from 'primeng/dialog';

@Component({
  selector: 'app-cart-details',
  templateUrl: './cart-details.component.html',
  styleUrls: ['./cart-details.component.css']
})
export class CartDetailsComponent implements OnInit {
  cart: Cart;
  cartCount: number;
  cartDetails: CartDetails;
  product: Product;
  productFeature: ProductTypeFeature[];
  showSubCategory = false;
  productThumbnailUrl = environment.srcUrlProductThumbnail;
  featureProductUrl = environment.featureProductUrl;
  display: boolean;
  isLoggedin: string;
  constructor(
    private router: Router, private activatedRoute: ActivatedRoute,
    private cartService: CartService) {
    this.cart = {
      ID: 0, UserID: 0, GuidID: '', Active: false, Subtotal: 0, Total: 0, TotalTax: 0,
      ListCartDetails: [{
        ID: 0, CartID: 0, ProductID: 0, ProductName: '', ProductTypeID: 0, Price: '', SalePrice: '', Quantity: 0, productDetail: {
          ID: 0, ProductName: '', ProductDescription: '', SalePrice: '', Price: '', ProductImage: '', ProductTypeID: 0, CourseID: 0,
          ProgramID: 0, Active: false, ListProductTypeFeature: [{
            ID: 0, DisplayName: '', FeatureImage: '', FeatureTypeID: 0, FeatureTypeName: '', IsDisplayToEndUser: false, Name: '',
            ProductTypeID: 0, ProductTypeFeatureID: 0, ListProductFeatureValue: [{ ID: 0, OptionName: '', Value: '', OptionID: 0 }]
          }], ModifiedDate: new Date(),
          ProductCategories: [{ ID: 0, ProductID: 0, CategoryID: 0 }]
        }
      }]
    };
    this.product = {
      ID: 0, ProductName: '', ProductDescription: '', SalePrice: '', Price: '', ProductImage: '', ProductTypeID: 0, CourseID: 0,
      ProgramID: 0, Active: false, ListProductTypeFeature: [{
        ID: 0, DisplayName: '', FeatureImage: '', FeatureTypeID: 0, FeatureTypeName: '', IsDisplayToEndUser: false, Name: '',
        ProductTypeID: 0, ProductTypeFeatureID: 0, ListProductFeatureValue: [{ ID: 0, OptionName: '', Value: '', OptionID: 0 }]
      }], ModifiedDate: new Date(),
      ProductCategories: [{ ID: 0, ProductID: 0, CategoryID: 0 }]
    };
    this.productFeature = [{
      ID: 0, DisplayName: '', FeatureImage: '', FeatureTypeID: 0, FeatureTypeName: '', IsDisplayToEndUser: false, Name: '',
      ProductTypeID: 0, ProductTypeFeatureID: 0, ListProductFeatureValue: [{ ID: 0, OptionName: '', Value: '', OptionID: 0 }]
    }];

    this.cartDetails = {
      ID: 0, CartID: 0, ProductID: 0, ProductName: '', ProductTypeID: 0, Price: '', SalePrice: '', Quantity: 0, productDetail: this.product
    };
  }

  ngOnInit() {
    this.getCartDetails();
  }

  loginEvent($event) {
    console.log('loginPopUpOut', $event);
    this.display = $event;
    this.isLoggedin = 'true';
  }

  getCartDetails() {
    this.cartService.getCartDetails().subscribe((data: Cart) => {
      this.cart = data;
      if (this.cart.ListCartDetails != null) {
        this.cartCount = this.cart.ListCartDetails.length;
      }
      console.log('this.cart', this.cart);
    });
  }

  removeFromCart(cartDetails: CartDetails) {
    this.cartService.DeleteFromCartDetails(cartDetails).then((result: any) => {
      this.getCartDetails();
    });
  }

  removeQuantity(cartDetails: CartDetails) {
    cartDetails.Quantity = cartDetails.Quantity - 1;
    this.cartService.UpdateCartDetails(cartDetails).then((result: any) => {
      this.getCartDetails();
    });
  }

  addQuantity(cartDetails: CartDetails) {
    cartDetails.Quantity = cartDetails.Quantity + 1;
    this.cartService.UpdateCartDetails(cartDetails).then((result: any) => {
      this.getCartDetails();
    });
  }
  proceedToBuy(url, id) {
    const addressUrl = `${url}/${id}`;
    const isLoggedin = localStorage.getItem('isLoggedin');
    console.log('isLoggedin: ', isLoggedin);
    if (isLoggedin === 'true') {
      this.router.navigateByUrl(addressUrl).then(e => {
      });
    } else {
      this.display = true;
    }

  }

  viewProduct(url, id) {
    const productUrl = `${url}/${id}`;
    console.log('productUrl: ', productUrl);
    this.router.navigateByUrl(productUrl).then(e => {
      if (e) {
      } else {
      }
    });
  }
}
