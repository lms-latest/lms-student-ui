import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../shared/services/user.service';
import { UserPersonalInfo } from '../../shared/interface/userPersonalInfo';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.css']
})
export class MyProfileComponent implements OnInit {

  isPersonalInfo: boolean;
  isEmailAddress: boolean;
  isMobileNumber: boolean;
  userInfo: UserPersonalInfo;
  constructor(private userService: UserService, private router: Router) {
    this.isPersonalInfo = true;
    this.isEmailAddress = true;
    this.isMobileNumber = true;
    this.userInfo = { ID: 0, FirstName: '', LastName: '', EmailAddress: '', MobileNumber: '', Password: '', Active: false };
  }

  ngOnInit() {
    // this.isPersonalInfo = false;
    this.getUserInfo();
  }
  getUserInfo() {
    this.userService.GetUserInfo().subscribe((data: any) => {
      console.log('getUserInfo', data);
      this.userInfo.ID = data.ID;
      this.userInfo.FirstName = data.FirstName;
      this.userInfo.LastName = data.LastName;
      this.userInfo.EmailAddress = data.email;
      this.userInfo.MobileNumber = data.mobileNo;
      this.userInfo.Password = data.password;
      this.userInfo.Active = data.IsActive;
    });
  }
  personalInfo() {
    this.isPersonalInfo = false;
  }

  personalInfoSave() {
    this.isPersonalInfo = true;
    this.userService.UpdatePersonalInfo(this.userInfo).then((data: any[]) => {
    });
  }

  emailAddress() {
    this.isEmailAddress = false;
  }

  emailAddressSave() {
    this.isEmailAddress = true;
    this.userService.UpdatePersonalInfo(this.userInfo).then((data: any[]) => {
    });
  }

  mobileNumber() {
    this.isMobileNumber = false;
  }

  mobileNumberSave() {
    this.isMobileNumber = true;
    this.userService.UpdatePersonalInfo(this.userInfo).then((data: any[]) => {
    });
  }

  deactivateAccount() {
    this.userService.deactivateAccount(this.userInfo).then((result: boolean) => {
      if (result === true) {
        localStorage.removeItem('authToken');
        localStorage.removeItem('userObject');
        localStorage.removeItem('currentUser');
        localStorage.removeItem('isLoggedin');
        this.router.navigate(['']);
      }
    });
  }
}
