import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentFailComponent } from './payment-fail.component';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    component: PaymentFailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentFailRoutingModule { }

