import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentFailRoutingModule } from './payment-fail-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PaymentFailRoutingModule
  ]
})
export class PaymentFailModule { }
