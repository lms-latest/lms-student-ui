import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MycourceComponent } from './mycource.component';

describe('MycourceComponent', () => {
  let component: MycourceComponent;
  let fixture: ComponentFixture<MycourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MycourceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MycourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
