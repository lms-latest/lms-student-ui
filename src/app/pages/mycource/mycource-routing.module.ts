import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MycourceComponent } from './mycource.component';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    component: MycourceComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MycourceRoutingModule { }
