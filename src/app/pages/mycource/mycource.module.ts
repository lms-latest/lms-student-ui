import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MycourceRoutingModule } from './mycource-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MycourceRoutingModule
  ]
})
export class MycourceModule { }


