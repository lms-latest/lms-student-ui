import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CourseService } from '../../shared/services/course.service';
import { ProductService } from '../../shared/services/product.service';
import { TreeNode } from 'primeng/api';
import { CarouselModule } from 'primeng/carousel';
import { BrowserModule } from '@angular/platform-browser';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';
import { Course } from '../../shared/interface/course';
import { Category } from '../../shared/interface/category';
import { Topic } from '../../shared/interface/topic';
import { Product, ProductCategory, ProductFeatureValue, ProductTypeFeature } from '../../shared/interface/product';




@Component({
  selector: 'app-mycource',
  templateUrl: './mycource.component.html',
  styleUrls: ['./mycource.component.css']
})
export class MycourceComponent implements OnInit {
  courseImageUrl = environment.courseImageUrl;
  categoryImageUrl = environment.categoryImageUrl;
  srcUrlCourseThumbnail = environment.srcUrlCourseThumbnail;
  safeSrc: SafeResourceUrl;
  pdfSrc: string;
  myurl: string;
  showSubCategory: boolean;
  showCourse: boolean;
  error = '';
  selectedCategory: Category;
  selectedCourse: Course;
  selectedAllCourse: Course;
  viewCourseObject = { CategoryID: 0, CourseID: 0 };
  userCourse: Course;
  userProduct: Product[];
  allCategory: Category[];
  category: Category[];
  rootCategory: Category[];
  nestedCategory: Category[];
  subCategory: Category[];
  allProduct: Product;
  productCategory: ProductCategory;
  learnCourse: Course[];
  subscribeCourse: Course[];
  topic: Topic;
  selectedProduct: Product;
  selectedProductFeature: ProductTypeFeature[];
  srcUrlProductCategory = environment.srcUrlProductCategory;
  featureProductUrl = environment.featureProductUrl;

  constructor(private courseService: CourseService, private router: Router, private productService: ProductService) {
    // this.allCourse = [];
    this.learnCourse = [];
    this.allCategory = [];
    this.category = [];
    this.rootCategory = [];
    this.nestedCategory = [];
    this.subCategory = [];
    this.selectedCourse = {
      ID: 0, AuthorName: '', ModifiedDate: new Date(), CreatedDate: new Date(), CourseDescription: '', CourseImage: '', CourseTitle: '',
      CourseShortName: '', Active: false, CreatedBy: 0, EndDate: new Date(), Hours: '', Lectures: 0, LevelsID: 0, ModifiedBy: 0,
      OriginalPrice: '', Rating: '', SalePrice: '', StartDate: new Date(), SelectedTopic: this.topic
    };
    this.selectedAllCourse = {
      ID: 0, AuthorName: '', ModifiedDate: new Date(), CreatedDate: new Date(), CourseDescription: '', CourseImage: '', CourseTitle: '',
      CourseShortName: '', Active: false, CreatedBy: 0, EndDate: new Date(), Hours: '', Lectures: 0, LevelsID: 0, ModifiedBy: 0,
      OriginalPrice: '', Rating: '', SalePrice: '', StartDate: new Date(), SelectedTopic: this.topic
    };
    this.userCourse = {
      ID: 0, AuthorName: '', ModifiedDate: new Date(), CreatedDate: new Date(), CourseDescription: '', CourseImage: '', CourseTitle: '',
      CourseShortName: '', Active: false, CreatedBy: 0, EndDate: new Date(), Hours: '', Lectures: 0, LevelsID: 0, ModifiedBy: 0,
      OriginalPrice: '', Rating: '', SalePrice: '', StartDate: new Date(), SelectedTopic: this.topic
    };
    this.selectedCategory = {
      ID: 0, Name: '', CategoryName: '', CategoryCode: '', IconName: '', ImageSmall: '', ImageMedium: '', ImageLarge: '',
      ParentCategoryID: 0, ParentName: ''
    };

    this.selectedProduct = {
      ID: 0, ProductName: '', ProductDescription: '', SalePrice: '', Price: '', ProductImage: '', ProductTypeID: 0, CourseID: 0,
      ProgramID: 0, Active: false, ListProductTypeFeature: this.selectedProductFeature, ModifiedDate: new Date(),
      ProductCategories: [{ ID: 0, ProductID: 0, CategoryID: 0 }]
    };

    this.selectedProductFeature = [{
      ID: 0, DisplayName: '', FeatureImage: '', FeatureTypeID: 0, FeatureTypeName: '', IsDisplayToEndUser: false, Name: '',
      ProductTypeID: 0, ProductTypeFeatureID: 0, ListProductFeatureValue: [{ ID: 0, OptionName: '', Value: '', OptionID: 0 }]
    }];
  }

  ngOnInit() {
    this.getAllCources();
    this.getAllCategory();
    this.getCourseSubscription();
    // this.getListOfProduct();
  }

  // getListOfProduct() {
  //   this.productService.getListOfProduct().subscribe((data: Product) => {
  //     this.allProduct = data;
  //     // this.learnCourse = data;
  //     console.log('getListOfProduct: ', data);
  //   });
  // }

  // getProductCategory() {
  //   this.productService.getProductCategory().subscribe((data: Category[]) => {
  //     this.allCategory = data;
  //     // this.learnCourse = data;
  //     console.log('getProductCategory: ', data);
  //   });
  // }

  viewCategory(url, id) {
    this.myurl = `${url}/${id}`;
    this.router.navigateByUrl(this.myurl).then(e => {
      if (e) {
      } else {
      }
    });
  }

  viewCourse(url, id) {
    this.myurl = `${url}/${id}`;
    this.router.navigateByUrl(this.myurl).then(e => {
      if (e) {
        this.saveVisitHistory(id);
      } else {
      }
    });
  }

  saveVisitHistory(courseID) {
    this.viewCourseObject.CourseID = courseID;
    this.courseService.saveVisitHistory(this.viewCourseObject).then((result: any) => {

    });
  }

  overlayEventSubcategory($event: any, category: Category) {
    this.selectedCategory = category;
    this.getChildCategory(category);
    console.log('this.subCategory.length: ', this.subCategory.length);
    if (this.subCategory.length > 0) {
      this.showSubCategory = true;
    } else {
      this.showSubCategory = false;
    }
  }

  overlayEventCourse(opCourse: any, $event: any, userCourse: Course) {
    this.selectedCourse = userCourse;
    opCourse.show($event);
  }

  subCategoryScreenHide() {
    this.showSubCategory = false;
  }

  courseScreenHide() {
    this.showCourse = false;
  }

  getAllCources() {
    this.courseService.getallcourse().subscribe((data: Course[]) => {
      console.log('getallcourse: ', data);
      // this.allCourse = data;
      this.learnCourse = data;
      // this.allCourse.forEach((r, i) => {
      // });
    });
  }

  overlayEventAllCourse(opAllCourse: any, $event: any, allCourse: Course) {
    this.selectedAllCourse = allCourse;
    opAllCourse.show($event);
  }

  getAllCategory() {
    this.courseService.getAllCategory().subscribe((data: Category[]) => {
      this.allCategory = data;
      console.log('allCategory: ', data);
      this.allCategory.forEach(x => {
        if (x.ParentCategoryID === 0) {
          this.rootCategory.push(x);
        }
      });
    });
  }

  getChildCategory(category) {
    console.log('category: ', category);
    this.subCategory = [];
    this.allCategory.forEach((obj, index) => {
      console.log('obj.ParentCategoryID: ', obj.ParentCategoryID);
      if (category.ID === obj.ParentCategoryID) {
        this.subCategory.push(obj);
      }
    });
  }

  getCourseSubscription() {
    this.courseService.getCourseSubscription().subscribe((data: Course[]) => {
      this.subscribeCourse = data;
    });
  }

  overlayEventAllProduct(opAllProduct: any, $event: any, product: Product) {
    // console.log('productFeatureValue', product);
    this.selectedProductFeature = product.ListProductTypeFeature;
    this.selectedProduct = product;
    opAllProduct.toggle($event);
  }

}
