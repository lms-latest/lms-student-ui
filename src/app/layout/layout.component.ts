import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { stringify } from '@angular/compiler/src/util';
import { SearchService } from '../shared/services/search.service';
import { DialogModule } from 'primeng/dialog';
import { CartComponent } from '../pages/cart/cart.component';
import { MenuItem } from 'primeng/api';


@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  searchKey: string;
  selectedCategory: any = { searchKey: '' };
  searchurl: string;
  isLoggedin: string;
  display: boolean;
  userName: string;
  items: MenuItem[];

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private searchService: SearchService) {
    this.isLoggedin = localStorage.getItem('isLoggedin');
    console.log('this.isLoggedin', this.isLoggedin);
  }

  @ViewChild(CartComponent) cartrefresh: CartComponent;
  @ViewChild(CartComponent) cartreset: CartComponent;
  ngOnInit() {
    this.items = [
      { label: 'MyProfile', icon: 'pi pi-user', routerLink: '/myprofile' },
      { label: 'Cources', icon: 'pi pi-clone', routerLink: '/mycourse' },
      { label: 'Orders', icon: 'pi pi-money-bill', routerLink: '/orders' },
      { label: 'Address', icon: 'pi pi-list', routerLink: '/address' },
      {
        label: 'Logout', icon: 'pi pi-sign-out', command: () => {
          this.logoutEvent();
        }
      }
    ];
}

loginEvent($event) {
  console.log('loginPopUpOut', $event);
  this.display = $event;
  this.isLoggedin = 'true';
  this.cartrefresh.getCartDetails();
}

logoutEvent() {
  console.log('logoutEvent');
  this.isLoggedin = 'false';
  localStorage.removeItem('authToken');
  localStorage.removeItem('userObject');
  localStorage.removeItem('currentUser');
  localStorage.removeItem('isLoggedin');
  localStorage.removeItem('isNonRegisterUser');
  this.cartreset.resetCartCount();
  // this.display = false;
  this.router.navigate(['']);
}

viewSearch(url, searchKey) {
  this.searchurl = `${url}/${searchKey}`;
  if (this.router.url.toLowerCase().startsWith('/searchresult')) {
    this.searchService.searchTerm.next(searchKey);
  }

  this.router.navigateByUrl(this.searchurl).then(e => {
  });
}

login() {
  this.display = true;
}
}
