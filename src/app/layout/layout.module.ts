import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from '../layout/layout.component';
import { LayoutRoutingModule } from './layout-routing.module';
import {InputTextModule} from 'primeng/inputtext';



@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        InputTextModule
    ]
})

export class LayoutModule {}
