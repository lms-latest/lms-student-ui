import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { HomeComponent} from '../pages/home/home.component';


const routes: Routes = [
  {
      path: '',
      component: LayoutComponent,
      children: [
         { path: '', redirectTo: 'home', pathMatch: 'prefix' },
         { path: 'home', loadChildren: '../pages/home/home.module#HomeModule' },
         { path: 'mycourse', loadChildren: '../pages/mycource/mycource.module#MycourceModule' },
         { path: 'myprofile', loadChildren: '../pages/my-profile/my-profile.module#MyProfileModule' },
         { path: 'aboutus', loadChildren: '../pages/aboutus/aboutus.module#AboutusModule' },
         { path: 'registration', loadChildren: '../registration/registration.module#RegistrationModule' },
         { path: 'coursedetails/:id', loadChildren: '../pages/course-details/course-details.module#CourseDetailsModule'},
         { path: 'productdetails/:id', loadChildren: '../pages/product-details/product-details.module#ProductDetailsModule'},
         { path: 'viewcart/:id', loadChildren: '../pages/viewcart/viewcart.module#ViewcartModule'},
         { path: 'searchresult/:key', loadChildren: '../pages/searchresult/searchresult.module#SearchresultModule' },
         { path: 'cart', loadChildren: '../pages/cart/cart.module#CartModule' },
         { path: 'cartdetails', loadChildren: '../pages/cart-details/cart-details.module#CartDetailsModule' },
         { path: 'address', loadChildren: '../pages/address/address.module#AddressModule'},
         { path: 'orders', loadChildren: '../pages/orders/orders.module#OrdersModule'},
         { path: 'orderdetails/:id', loadChildren: '../pages/order-details/order-details.module#OrderDetailsModule'},
         { path: 'address/:id', loadChildren: '../pages/address/address.module#AddressModule'},
         { path: 'checkout/:id', loadChildren: '../pages/checkout/checkout.module#CheckoutModule'},
         { path: 'paymentfail/:aid/:cid', loadChildren: '../pages/payment-fail/payment-fail.module#PaymentFailModule' },
         { path: 'paymentsuccess/:aid/:cid', loadChildren: '../pages/payment-success/payment-success.module#PaymentSuccessModule' },
      ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class LayoutRoutingModule { }
