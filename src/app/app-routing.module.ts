import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



// const routes: Routes = [
//   // {path: 'home', component: HomeComponent},
//   {path: '', component: LoginComponent},
//   // {path: 'dashboard', component: DashboardComponent},
//   // {path: 'mycource', component: MycourceComponent},
//   // {path: 'services', component: ServicesComponent},
//   // {path: 'aboutus', component: AboutusComponent},
//   // {path: 'cource/:id', component: CourceComponent},
//   {path: 'home', component: LayoutComponent}
// ];

const routes: Routes = [
  { path: '', loadChildren: './layout/layout.module#LayoutModule' },
//  { path: '', loadChildren: './login/login.module#LoginModule' },
  // { path: 'signup', loadChildren: './signup/signup.module#SignupModule' },
  // { path: 'not-found', loadChildren: './not-found/not-found.module#NotFoundModule' },
  // { path: '**', redirectTo: 'not-found' }
];


@NgModule({
  imports: [RouterModule.forRoot(routes),
  RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
