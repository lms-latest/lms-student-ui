import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login.component';


// const routes: Routes = [
//   {
//       path: '',
//       component: LoginComponent
//   }
// ];

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
    children: [
    // { path: '', loadChildren: './layout/layout.module#LayoutModule'},
    // { path: '', loadChildren: './login/login.module#LoginModule' },
    // { path: 'signup', loadChildren: './signup/signup.module#SignupModule' },
    // { path: 'not-found', loadChildren: './not-found/not-found.module#NotFoundModule' },
    // { path: '**', redirectTo: 'not-found' }
    ]
  }
];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }





