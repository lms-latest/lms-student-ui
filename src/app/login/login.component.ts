import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthloginService } from '../shared/services/authlogin.service';
import { JSEncrypt } from 'jsencrypt';
import { CryptoService } from '../shared/services/crypto.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  userLogin: any = { Username: '', Password: '' };
  userRegister: any = { firstname: '', lastname: '', email: '', mobileno: '', password: '' };
  result: any = [];
  error = '';
  Error = true;
  display: boolean;
  isRegistration: boolean;
  nonRegisterUser: string;
  @Output() loginEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor(private authloginService: AuthloginService, private router: Router, private cryptoService: CryptoService) { }

  ngOnInit() {
    this.isRegistration = false;
  }

  loginUser(event) {
    console.log('loginUser : ', event);
    event.preventDefault();
    const target = event.target;
    const username = target.querySelector('#username').value;
    const password = target.querySelector('#password').value;
    this.userLogin.Username = username;
    const encryptpassword = this.cryptoService.cryptoencrypt(
      password
    );
    this.userLogin.Password = encryptpassword;
    // this.authloginService.login(username, password);
    this.authloginService.login(this.userLogin).then((data: any[]) => {
      console.log('this.data : ', data);
      if (data == null) {
        this.Error = false;
      }
      this.result = data;
      localStorage.setItem('authToken', this.result.Token);
      localStorage.setItem('userObject', JSON.stringify(data));
      localStorage.setItem('currentUser', JSON.stringify(data));
     // localStorage.setItem('username', JSON.stringify(data.userName));
      localStorage.setItem('isLoggedin', 'true');
      localStorage.setItem('isNonRegisterUser', 'false');
      console.log('localStorage.getItem: userObject', localStorage.getItem('userObject'));
      console.log('localStorage.getItem: currentUser', localStorage.getItem('currentUser'));
      this.display = false;
      this.loginEvent.emit(this.display);
      // this.router.navigate(['/home']);
    });
  }

  // getOwnCources() {
  //   this.courseService.getowncourse(1).subscribe((data: any[]) => {
  //     this.userCourse = data;
  //     this.userCourse.forEach((r, i) => {
  //     });
  //   });
  // }

  userRegistration(event) {
    event.preventDefault();
    const target = event.target;
    this.userRegister.firstname = target.querySelector('#firstname').value;
    this.userRegister.lastname = target.querySelector('#lastname').value;
    this.userRegister.password = target.querySelector('#npassword').value;
    this.userRegister.email = target.querySelector('#email').value;
    this.userRegister.mobileno = target.querySelector('#mobileno').value;
    console.log('this.userRegister : ', this.userRegister);
    this.authloginService.userRegistration(this.userRegister).then((data: any[]) => {
      if (data == null) {
        this.Error = false;
      }
      this.result = data;
      console.log('userRegistration  return  result', data);
      this.display = false;
      this.loginEvent.emit(this.display);
      this.nonRegisterUser = localStorage.getItem('isNonRegisterUser');
      if (this.nonRegisterUser === 'true') {
        this.router.navigate(['/address']);
      }
      else {
        this.router.navigate(['/home']);
      }

    });
  }


  createAccount() {
    this.isRegistration = true;
    // this.router.navigate(['/registration']);
    // $('.message a').click(function () {
    //   $('form').animate({ height: "toggle", opacity: "toggle" }, "slow");
    // });

  }

  login() {
    this.isRegistration = false;
  }


}
