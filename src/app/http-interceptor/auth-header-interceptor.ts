import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';


@Injectable()

export class AuthHeaderInterceptor implements HttpInterceptor {

    intercept(request: HttpRequest<any>, next: HttpHandler) {
        const authToken = localStorage.getItem('authToken');
        // console.log('authToken in  Intercepter: ', authToken);
        if (authToken == null) {
           // console.log('IF');
            // const authReq = request.clone({
            //     setHeaders: {'Content-Type': 'application/json'}

            // });
            const authReq = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
            return next.handle(authReq);
        } else {
       // console.log('ELSE');
        const authReq = request.clone({
            setHeaders: { Authorization: 'Bearer ' + authToken}
        });

        return next.handle(authReq);
        }
    }
}
