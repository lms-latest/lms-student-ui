import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthloginService } from '../shared/services/authlogin.service';
import { JSEncrypt } from 'jsencrypt';
import { CryptoService } from '../shared/services/crypto.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  isRegistration: boolean;
  constructor(private authloginService: AuthloginService, private router: Router, private cryptoService: CryptoService) { }

  ngOnInit(): void {
  }
  login() {
    this.isRegistration = false;
  }
}
