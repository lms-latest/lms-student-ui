import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { LayoutModule } from './layout/layout.module';
import { LayoutRoutingModule } from './layout/layout-routing.module';
import { LoginModule } from './login/login.module';
import { LoginRoutingModule } from './login/login-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { MycourceComponent } from './pages/mycource/mycource.component';
import { MatToolbarModule, MatButtonModule, MatIconModule, MatMenuModule, MatSidenavModule } from '@angular/material';
import { AboutusComponent } from './pages/aboutus/aboutus.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatBadgeModule } from '@angular/material/badge';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatStepperModule } from '@angular/material/stepper';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTreeModule } from '@angular/material/tree';
import { TreeModule } from 'primeng/tree';
import { TreeTableModule } from 'primeng/treetable';
import { CourceComponent } from './pages/cource/cource.component';
import { CarouselModule } from 'primeng/carousel';
import { GalleriaModule } from 'primeng/galleria';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { ListboxModule } from 'primeng/listbox';
import { TableModule } from 'primeng/Table';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { httpInterceptProviders } from './http-interceptor';
import { SearchresultComponent } from './pages/searchresult/searchresult.component';
import { LayoutComponent } from './layout/layout.component';
import { HomeModule } from './pages/home/home.module';
import { HomeRoutingModule } from './pages/home/home-routing.module';
import { MycourceRoutingModule } from './pages/mycource/mycource-routing.module';
import { MycourceModule } from './pages/mycource/mycource.module';
import { AboutusRoutingModule } from './pages/aboutus/aboutus-routing.module';
import { AboutusModule } from './pages/aboutus/aboutus.module';
import { LogoutComponent } from './pages/logout/logout.component';
import { LogoutRoutingModule } from './pages/logout/logout-routing.module';
import { LogoutModule } from './pages/logout/logout.module';
import { CourceRoutingModule } from './pages/cource/cource-routing.module';
import { CourceModule } from './pages/cource/cource.module';
import { SearchresultRoutingModule } from './pages/searchresult/searchresult-routing.module';
import { SearchresultModule } from './pages/searchresult/searchresult.module';
import { DataViewModule } from 'primeng/dataview';
import { SearchService } from '../app/shared/services/search.service';
import { CourseDetailsComponent } from './pages/course-details/course-details.component';
import { CourseDetailsRoutingModule } from './pages/course-details/course-details-routing.module';
import { CourseDetailsModule } from './pages/course-details/course-details.module';
import { AccordionModule } from 'primeng/accordion';
import { CheckboxModule } from 'primeng/checkbox';
import { SafePipe } from './shared/pipes/safe.pipe';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { QuizPreviewComponent } from './pages/quiz-preview/quiz-preview.component';
import { RadioButtonModule } from 'primeng/radiobutton';
import { PaginatorModule } from 'primeng/paginator';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { CountdownTimerModule } from 'ngx-countdown-timer';
import { CountdownModule } from 'ngx-countdown';
import { ThankyouComponent } from './pages/thankyou/thankyou.component';
import { ShowResultComponent } from './pages/show-result/show-result.component';
import { ProductDetailsComponent } from './pages/product-details/product-details.component';
import { ProductDetailsRoutingModule } from './pages/product-details/product-details-routing.module';
import { ProductDetailsModule } from './pages/product-details/product-details.module';
import { ViewcartComponent } from './pages/viewcart/viewcart.component';
import { ViewcartModule } from './pages/viewcart/viewcart.module';
import { ViewcartRoutingModule } from './pages/viewcart/viewcart-routing.module';
import { CartComponent } from './pages/cart/cart.component';
import { CartDetailsComponent } from './pages/cart-details/cart-details.component';
import { CartModule } from './pages/cart/cart.module';
import { CartRoutingModule } from './pages/cart/cart-routing.module';
import { CartDetailsRoutingModule } from './pages/cart-details/cart-details-routing.module';
import { CartDetailsModule } from './pages/cart-details/cart-details.module';
import { AddressComponent } from './pages/address/address.component';
import { AddressRoutingModule } from './pages/address/address-routing.module';
import { AddressModule } from './pages/address/address.module';
import { CheckoutComponent } from './pages/checkout/checkout.component';
import { CheckoutModule } from './pages/checkout/checkout.module';
import { CheckoutRoutingModule } from './pages/checkout/checkout-routing.module';
import { PaymentSuccessComponent } from './pages/payment-success/payment-success.component';
import { PaymentFailComponent } from './pages/payment-fail/payment-fail.component';
import { PaymentSuccessRoutingModule } from './pages/payment-success/payment-success-routing.module';
import { PaymentSuccessModule } from './pages/payment-success/payment-success.module';
import { PaymentFailRoutingModule } from './pages/payment-fail/payment-fail-routing.module';
import { PaymentFailModule } from './pages/payment-fail/payment-fail.module';
import { DialogModule } from 'primeng/dialog';
import { OrdersComponent } from './pages/orders/orders.component';
import { OrdersRoutingModule } from './pages/orders/orders-routing.module';
import { OrdersModule } from './pages/orders/orders.module';
import { OrderDetailsComponent } from './pages/order-details/order-details.component';
import { OrderDetailsRoutingModule } from './pages/order-details/order-details-routing.module';
import { OrderDetailsModule } from './pages/order-details/order-details.module';
import { SelectButtonModule } from 'primeng/selectbutton';
import { RegistrationComponent } from './registration/registration.component';
import { RegistrationModule } from './registration/registration.module';
import { RegistrationRoutingModule } from './registration/registration-routing.module';
import { MyProfileComponent } from './pages/my-profile/my-profile.component';
import { MyProfileRoutingModule } from './pages/my-profile/my-profile-routing.module';
import { MyProfileModule } from './pages/my-profile/my-profile.module';
import { TooltipModule } from 'primeng/tooltip';
import { StripTagsPipe } from './shared/pipes/strip-tags.pipe';
import { MenuModule } from 'primeng/menu';
import { NgCircleProgressModule } from 'ng-circle-progress';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    DashboardComponent,
    MycourceComponent,
    AboutusComponent,
    CourceComponent,
    SearchresultComponent,
    LayoutComponent,
    LogoutComponent,
    CourseDetailsComponent,
    SafePipe,
    QuizPreviewComponent,
    ThankyouComponent,
    ShowResultComponent,
    ProductDetailsComponent,
    ViewcartComponent,
    CartComponent,
    CartDetailsComponent,
    AddressComponent,
    CheckoutComponent,
    PaymentSuccessComponent,
    PaymentFailComponent,
    OrdersComponent,
    OrderDetailsComponent,
    RegistrationComponent,
    MyProfileComponent,
    StripTagsPipe
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    LayoutModule,
    LayoutRoutingModule,
    HttpClientModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatSidenavModule,
    BrowserAnimationsModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    TreeModule,
    TreeTableModule,
    CarouselModule,
    GalleriaModule,
    ScrollPanelModule,
    ListboxModule,
    TableModule,
    DynamicDialogModule,
    OverlayPanelModule,
    ButtonModule,
    InputTextModule,
    LoginModule,
    LoginRoutingModule,
    HomeModule,
    HomeRoutingModule,
    MycourceRoutingModule,
    MycourceModule,
    AboutusRoutingModule,
    AboutusModule,
    LogoutRoutingModule,
    LogoutModule,
    CourceRoutingModule,
    CourceModule,
    SearchresultRoutingModule,
    SearchresultModule,
    DataViewModule,
    CourseDetailsRoutingModule,
    CourseDetailsModule,
    AccordionModule,
    CheckboxModule,
    NgxExtendedPdfViewerModule,
    RadioButtonModule,
    PaginatorModule,
    ConfirmDialogModule,
    CountdownModule,
    ProductDetailsRoutingModule,
    ProductDetailsModule,
    ViewcartRoutingModule,
    ViewcartModule,
    CartModule,
    CartRoutingModule,
    CartDetailsRoutingModule,
    CartDetailsModule,
    AddressRoutingModule,
    AddressModule,
    CheckoutModule,
    CheckoutRoutingModule,
    PaymentSuccessRoutingModule,
    PaymentSuccessModule,
    PaymentFailRoutingModule,
    PaymentFailModule,
    DialogModule,
    OrdersRoutingModule,
    OrdersModule,
    OrderDetailsRoutingModule,
    OrderDetailsModule,
    SelectButtonModule,
    RegistrationModule,
    RegistrationRoutingModule,
    MyProfileRoutingModule,
    MyProfileModule,
    TooltipModule,
    MenuModule,
    NgCircleProgressModule.forRoot({
      // radius: 40,
      // space: -10,
      // outerStrokeGradient: true,
      // outerStrokeWidth: 10,
      // outerStrokeColor: '#4882c2',
      // outerStrokeGradientStopColor: '#53a9ff',
      // innerStrokeColor: '#e7e8ea',
      // innerStrokeWidth: 0,
      // animateTitle: false,
      // animationDuration: 1000,
      // showUnits: false,
      // showBackground: false,
      // clockwise: false,
      // startFromZero: false
    })
  ],
  exports: [SafePipe],
  providers: [httpInterceptProviders, SearchService, ConfirmationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
