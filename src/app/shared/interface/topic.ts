export interface Topic {
    ID: number;
    CreatedBy: number;
    CreatedDate: Date;
    ModifiedBy: number;
    ModifiedDate: Date;
    Active: boolean;
    CourseID: number;
    TopicTitle: string;
    TopicDescription: string;
    TopicAttachment: string;
    TopicPath: string;
    ParentTopicID: number;
    TopicFileName: string;
    TopicType: string;
    QuizID: number;
    ParentTopicRefID: string;
    NodeRefID: string;
}

