export interface TopicPacket {
    ID: number;
    CourseID: number;
    TopicID: number;
    TopicDuration: number;
    TopicStatus: boolean;

}
