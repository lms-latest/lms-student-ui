import { Data } from '@angular/router';

export class Product {
    ID: number;
    ProductName: string;
    ProductDescription: string;
    SalePrice: string;
    Price: string;
    ProductImage: string;
    ProductTypeID: number;
    CourseID: number;
    ProgramID: number;
    Active: boolean;
    ModifiedDate: Data;
    ProductCategories: ProductCategory[];
    ListProductTypeFeature: ProductTypeFeature[];
}

export class ProductType {
    ID: number;
    Name: string;
}
export class ProductCategory {
    ID = 0;
    ProductID = 0;
    CategoryID = 0;
}


export class ProductTypeFeature {
    ID: number;
    Name: string;
    ProductTypeID: number;
    FeatureTypeID: number;
    FeatureTypeName: string;
    ProductTypeFeatureID: number;
    DisplayName: string;
    IsDisplayToEndUser: boolean;
    FeatureImage: string;
    ListProductFeatureValue: ProductFeatureValue[];
}

export class ProductFeatureValue {
    ID: number;
    Value: string;
    OptionID: number;
    OptionName: string;
}


export class SelectedListOptions {
    ID: number;
    ProductTypeFeatureID: number;
    Name: string;
}
