export interface TreeTableNode {
   data: { ID: number;
    label: string;
    data: string;
    type: string;
    IconName: string;
   };
    children?: TreeTableNode[];
}
