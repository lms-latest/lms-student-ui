import { Address } from './address';

export class Order {
    ID: number;
    OrderDate: Date;
    Total: number;
    SubTotal: number;
    StatusID: number;
    CustomerID: number;
    OrderAddressID: number;
    BillingAddressID: number;
    DeliveryAddressID: number;
    PaymentMethodID: number;
    Notes: string;
    Tax: number;
    SameAsBilling: boolean;
    OrderDetails: OrderDetail[];
    Addresses: Address[];
}
export class OrderDetail {
    ID: number;
    ProductName: string;
    ProductID: number;
    Quantity: string;
    LineTotal: number;
    OrderID: number;
    Price: string;
    Active: boolean;
    IsTaxInclusiveInPrice: boolean;
    TaxPercentage: string;
    TaxLabel: string;
    LineTax: number;
    ProductImage: string;
}
export class OrderStatus {
    ID: number;
    Status: string;
}
