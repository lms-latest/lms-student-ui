
export class Quiz {
    ID: number;
    PassingMarks: string;
    QuizTitle: string;
    QuestionPerPage: number;
    IsMarkApplicable: boolean;
    QuizDuration: number;
    QuestionList: Question[];
    ShowCorrectAnswer: boolean;
    ShowResultAfterSubmission: boolean;
    isQuizSubmission: boolean;
    QuizSubmissionDuration: number;
    QuizStartOn: Date;
    QuizEndOn: Date;
    TotalMarks: number;
    GetMarks: number;
}

export class Question {
    ID: number;
    QuestionTitle: string;
    HelpText: string;
    Marks: string;
    IsRequired: boolean;
    QuestionTypeID: number;
    QuizID: number;
    QuestionOptions: QuestionOption[];
    RefID: string;
    Active: boolean;
    Answer: Answer;
}

export class Answer {
    AnswerSingle: number;
    AnswerText: string;
    AnswerMultiple: number[];
}


export class QuestionOption {
    ID: number;
    Name: string;
    IsCorrect: boolean;
    QuestionID: number;
    QuestionRefID: string;
    Active: boolean;
    IsChecked: boolean;
}
export class QuestionType {
    ID: number;
    Name: string;
}
