
export interface Address {
    ID: number;
    FullName: string;
    MobileNumber: string;
    AlternatPhoneNumber: string;
    Pincode: string;
    HouseBuildingCompanyApartment: string;
    AreaColonyStreetSectorVillage: string;
    Landmark: string;
    TownCityDistrict: string;
    State: string;
    Country: string;
    CountryID: number;
    StateID: number;
    AddressTypeID: number;
    IsDefaultAddress: boolean;
}

export interface AddressType {
    ID: number;
    Name: string;
}

export interface AddressBasket {
    ListAddress: Address[];
    addressType: AddressType[];
}

export interface State {
    ID: number;
    Name: string;
    CountryID: number;
}

export interface Country {
    ID: number;
    Name: string;
    CountryISOCode: string;
    CountryISDCode: string;
}

