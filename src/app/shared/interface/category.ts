export interface Category {
    ID: number;
    Name: string;
    CategoryName: string;
    IconName: string;
    CategoryCode: string;
    ImageSmall: string;
    ImageMedium: string;
    ImageLarge: string;
    ParentCategoryID: number;
    ParentName: string;
}
