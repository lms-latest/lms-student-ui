export interface ConfirmationDialog {
    severity: string;
    summary: string;
    detail: string;
}
