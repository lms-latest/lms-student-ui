export class TreeTopic {
    ID: number;
    label: string;
    data: string;
    type: string;
    IconName: string;

    children?: TreeTopic[];
}
