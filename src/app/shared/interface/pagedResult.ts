export interface PagedResult<T> {
    Result: T[];
    Count: number;
}
