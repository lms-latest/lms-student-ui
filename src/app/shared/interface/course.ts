import { decimalDigest } from '@angular/compiler/src/i18n/digest';
import { Time } from '@angular/common';
import { Topic } from './topic';

export interface Course {
    ID: number;
    CreatedBy: number;
    CreatedDate: Date;
    ModifiedBy: number;
    ModifiedDate: Date;
    Active: boolean;
    CourseTitle: string;
    AuthorName: string;
    CourseDescription: string;
    CourseShortName: string;
    StartDate: Date;
    EndDate: Date;
    SelectedTopic: Topic;
    SalePrice: string;
    OriginalPrice: string;
    Rating: string;
    CourseImage: string;
    Hours: string;
    Lectures: number;
    LevelsID: number;
}

