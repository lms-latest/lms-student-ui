export interface UserPersonalInfo {
    ID: number;
    FirstName: string;
    LastName: string;
    MobileNumber: string;
    EmailAddress: string;
    Password: string;
    Active: boolean;
}
