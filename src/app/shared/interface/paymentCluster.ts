import { Address } from './address';
import { Cart } from './cart';
export interface PaymentCluster {
    ID: number;
    UserID: number;
    CardHolderName: string;
    CardNumber: string;
    CVV: string;
    ExpireMonth: string;
    ExpireYear: string;
    TransactionID: string;
    CardType: string;
    TotalAmount: string;
    Currency: string;
    PaymentMethod: string;
    AddressID: number;
    TransactionAmount: string;
    BillingAddress: Address;
    Cart: Cart;
}
