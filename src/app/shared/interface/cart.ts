import { Product } from './product';

export interface Cart {
    ID: number;
    UserID: number;
    GuidID: string;
    Active: boolean;
    Subtotal: number;
    TotalTax: number;
    Total: number;
    ListCartDetails: CartDetails[];
}


export interface CartDetails {
    ID: number;
    CartID: number;
    ProductID: number;
    ProductName: string;
    ProductTypeID: number;
    Price: string;
    SalePrice: string;
    Quantity: number;
    productDetail: Product;
}

export interface PaymentMethod {
    label: string;
    value: string;
}


