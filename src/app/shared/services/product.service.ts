import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private rootUrl = `${environment.apiBaseUrl}/api/Product`;
  constructor(private http: HttpClient) {
  }


  public getProduct(productID: number) {
    return this.http.get(this.rootUrl + '/GetProduct/' + productID);
  }


  public getCategoryList() {
    return this.http.get(this.rootUrl + '/GetCategoryList');
  }

  public getRecommendedCategory() {
    return this.http.get(this.rootUrl + '/GetRecommendedCategory');
  }

}
