import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { PaymentCluster } from '../../shared/interface/paymentCluster';
import * as uuid from 'uuid';


@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  private rootUrl = `${environment.apiBaseUrl}/api/payment`;
  constructor(private http: HttpClient) { }


  CheckoutPayment(paymentCluster: PaymentCluster) {
    paymentCluster.TransactionID = uuid.v4();
    return new Promise((resolve, reject) => {
      this.http.post(this.rootUrl + '/checkoutPayment', paymentCluster)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
}
