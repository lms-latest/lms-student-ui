import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Cart, CartDetails } from '../../shared/interface/cart';
import { BehaviorSubject } from 'rxjs';
import * as uuid from 'uuid';
@Injectable({
  providedIn: 'root'
})
export class CartService {
  private rootUrl = `${environment.apiBaseUrl}/api/cart`;
  cart: Cart;
  constructor(private http: HttpClient) {
    this.cart = {
      ID: 0, UserID: 0, GuidID: '', Active: false, Subtotal: 0, Total: 0, TotalTax: 0,
      ListCartDetails: [{
        ID: 0, CartID: 0, ProductID: 0, ProductName: '', ProductTypeID: 0, Price: '', SalePrice: '', Quantity: 0, productDetail: {
          ID: 0, ProductName: '', ProductDescription: '', SalePrice: '', Price: '', ProductImage: '', ProductTypeID: 0, CourseID: 0,
          ProgramID: 0, Active: false, ListProductTypeFeature: [{
            ID: 0, DisplayName: '', FeatureImage: '', FeatureTypeID: 0, FeatureTypeName: '', IsDisplayToEndUser: false, Name: '',
            ProductTypeID: 0, ProductTypeFeatureID: 0, ListProductFeatureValue: [{ ID: 0, OptionName: '', Value: '', OptionID: 0 }]
          }], ModifiedDate: new Date(),
          ProductCategories: [{ ID: 0, ProductID: 0, CategoryID: 0 }]
        }
      }]
    };
  }

  public Count: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  public getCartDetails() {
    const guidID = localStorage.getItem('guidID');
    return this.http.get(this.rootUrl + '/GetCartDetails/' + guidID);
  }

  public getPaymentMethod() {
    return this.http.get(this.rootUrl + '/GetPaymentMethod');
  }


  public getCartDetailsByID(cartID: number) {
    return this.http.get(this.rootUrl + '/GetCartDetailsByID/' + cartID);
  }


  addToCart(cart: Cart) {
    const guidID = localStorage.getItem('guidID');
    console.log('guidID', guidID);
    if (guidID === null) {
      cart.GuidID = uuid.v4();
      localStorage.setItem('guidID', cart.GuidID);
    } else {
      cart.GuidID = guidID;
    }

    console.log('addToCart: ');
    return new Promise((resolve, reject) => {
      this.http.post(this.rootUrl + '/SaveCart', cart)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }


  DeleteFromCartDetails(cartDetails: CartDetails) {
    return new Promise((resolve, reject) => {
      this.http.post(this.rootUrl + '/DeleteFromCartDetails', cartDetails)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  UpdateCartDetails(cartDetails: CartDetails) {
    return new Promise((resolve, reject) => {
      this.http.post(this.rootUrl + '/UpdateCartDetails', cartDetails)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
}
