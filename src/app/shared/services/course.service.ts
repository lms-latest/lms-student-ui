import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CourseService {
  private isLogged$ = new BehaviorSubject(false);
  private url = `${environment.apiBaseUrl}/api/course`;
  private urlCategory = `${environment.apiBaseUrl}/api/Category`;
  constructor(private httpClient: HttpClient) { }

  public getAllSearchResult(searchKey: string, pageNumber: number, pageSize: number) {
    searchKey = searchKey.trim();
    if (searchKey === '') {
      searchKey = '(all)';
    }
    console.log('call getAllSearchResult service');
    return this.httpClient.get(this.url + '/GetAllSearchResult/' + searchKey + '/' + pageNumber + '/' + pageSize);
  }

  public getallcourse() {
    console.log('getallcourse');
    return this.httpClient.get(`${this.url}/getallcourse`);
    // const headers = new HttpHeaders().set('Access-Control-Allow-Origin', '*');
    // return this.http.get(`${this.url}/getallcourse`)
    //   .pipe(
    //    // map((res: { user: any, token: string }) => {
    //     map((res) => {
    //       console.log('getallcourse result',res)
    //       //this.isLogged$.next(true);
    //       return res;
    //     }));
  }


  public getCourseSubscription() {
    return this.httpClient.get(this.url + '/GetCourseSubscription');
  }


  public getAllCategory() {
    console.log('getAllCategory');
    return this.httpClient.get(this.urlCategory + '/GetListOfCategory');
  }


  public getCourseDetails(courseid: number) {
    console.log('GetCourseDetails', courseid);
    return this.httpClient.get(this.url + '/GetCourseDetails/' + courseid);
  }

  public getCourseTree(courseid: number) {
    return this.httpClient.get(this.url + '/GetCourseTree/' + courseid);
  }

  public getCourseInfo(courseid: number) {
    return this.httpClient.get(this.url + '/GetCourseInfo/' + courseid);
  }


  saveVisitHistory(viewCourseObject: any) {
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.url + '/SaveVisitHistory', viewCourseObject)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  saveTopicProgress(topicPacket: any) {
    alert('saveTopicProgress');
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.url + '/SaveTopicProgress', topicPacket)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

}
