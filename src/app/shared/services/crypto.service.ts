import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { catchError } from 'rxjs/operators';
// import { EditUser } from 'src/app/layout/editUser/editUser';
import { environment } from 'src/environments/environment';
import { JSEncrypt } from 'jsencrypt';

@Injectable({
  providedIn: 'root'
})
export class CryptoService {
  rootUrl: string;
  // private currentUserSubject: BehaviorSubject<EditUser>;
  // public currentUser: Observable<EditUser>;
  plainText: string;
  cypherText: string;
  JSEncrypt: any;
  constructor(private http: HttpClient) {
    // this.currentUserSubject = new BehaviorSubject<EditUser>(JSON.parse(localStorage.getItem('currentUser')));
    // this.currentUser = this.currentUserSubject.asObservable();
    this.rootUrl = environment.apiBaseUrl;
  }

  //   public get currentUserValue(): EditUser {
  //     return this.currentUserSubject.value;
  // }

  public cryptoencrypt(test: string) {
    // debugger;
    this.JSEncrypt = new JSEncrypt();
    this.JSEncrypt.setPublicKey(environment.cryptoPublicKey);
    this.cypherText = this.JSEncrypt.encrypt(test);
    return this.cypherText;

  }
}
