import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { PaymentCluster } from '../../shared/interface/paymentCluster';

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {
  private rootUrl = `${environment.apiBaseUrl}/api/address`;
  constructor(private http: HttpClient) { }


  SaveAddress(paymentCluster: PaymentCluster) {
    return new Promise((resolve, reject) => {
      this.http.post(this.rootUrl + '/SaveAddress', paymentCluster)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
}

