import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  private rootUrl = `${environment.apiBaseUrl}/api/Order`;
  constructor(private http: HttpClient) { }


  public getOrders() {
    console.log('OrdersService: ');
    return this.http.get(this.rootUrl + '/GetUserOrders');
  }

  public getOrderDetails(orderID: number) {
    return this.http.get(this.rootUrl + '/GetOrderDetails/' + orderID);
  }
}
