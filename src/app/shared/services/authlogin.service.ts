import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthloginService {
  private isLogged$ = new BehaviorSubject(false);
  private url = `${environment.apiBaseUrl}/api/Auth`;
  private urlRegister = `${environment.apiBaseUrl}/api/Home`;
  constructor(private httpClient: HttpClient) { }

  // getUsersDetails(){

  // }

  public get isLoggedIn(): boolean {
    return this.isLogged$.value;
  }

  public login(data: any) {
    console.log('AuthloginService', data);
    // const headers = new HttpHeaders().set('Authorization', 'Bearer ' + Token);

    return new Promise((resolve, reject) => {
      this.httpClient.post(this.url + '/Authenticate', data)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
   }

   public userRegistration(data: any) {
    console.log('userRegistration', data);
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.url + '/Registration', data)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
   }
}
