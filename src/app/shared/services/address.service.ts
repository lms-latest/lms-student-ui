import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Address, AddressType, AddressBasket } from '../../shared/interface/address';

@Injectable({
  providedIn: 'root'
})
export class AddressService {
  private rootUrl = `${environment.apiBaseUrl}/api/address`;
  constructor(private http: HttpClient) { }


  public getAddress() {
    return this.http.get(this.rootUrl + '/GetAddress' );
  }

  public getCountry() {
    return this.http.get(this.rootUrl + '/GetCountry' );
  }
  public getAddressType() {
    return this.http.get(this.rootUrl + '/GetAddressType' );
  }

  public getStateByCountryID(countryID: number) {
    return this.http.get(this.rootUrl + '/GetStateByCountry/' + countryID );
  }

  public getSelectedAddress(addressID: number) {
    return this.http.get(this.rootUrl + '/GetSelectedAddress/' + addressID);
  }


  SaveAddress(address: Address) {
    return new Promise((resolve, reject) => {
      this.http.post(this.rootUrl + '/SaveAddress', address)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  DeleteAddress(addressID: number) {
    return this.http.get(this.rootUrl + '/DeleteAddress/' + addressID );
  }

}
