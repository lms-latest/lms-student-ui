import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Quiz, QuestionType } from '../interface/quiz';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class QuizService {
  private rootUrl = `${environment.apiBaseUrl}/api/quiz`;
  private Quizs = new BehaviorSubject([]);
  private QuizList: Quiz[];
  constructor(private http: HttpClient) {
  }


  public getQuizByID(QuizID: number, courseID: number) {
   // console.log('service getQuizByID', QuizID);
    return this.http.get(this.rootUrl + '/GetQuiz/' + QuizID + '/' + courseID);
  }

  public getQuizSubmission(quizID: number, courseID: number) {
    console.log('getQuizByID', quizID);
    return this.http.get(this.rootUrl + '/GetQuizSubmission/' + '/' + quizID + '/' + courseID);
  }


  saveQuizResponseAnswer(responseAnswer: any) {
    return new Promise((resolve, reject) => {
      this.http.post(this.rootUrl + '/SaveQuizResponseAnswer', responseAnswer)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

}
