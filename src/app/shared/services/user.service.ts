import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private rootUrl = `${environment.apiBaseUrl}/api/User`;
  constructor(private http: HttpClient) { }

  public GetUserInfo() {
    console.log('GetUserInfo');
    return this.http.get(this.rootUrl + '/GetUserInfo');
  }

  UpdatePersonalInfo(userInfo: any) {
    return new Promise((resolve, reject) => {
      this.http.post(this.rootUrl + '/UpdatePersonalInfo', userInfo)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  deactivateAccount(userInfo: any) {
    return new Promise((resolve, reject) => {
      this.http.post(this.rootUrl + '/DeactivateAccount', userInfo)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
}
